<?php

class ClubdecorController extends Zend_Controller_Action
{

    public function init()
    {
        session_start();
        // validar se o usuario esta logado e se tem autorização para acessar este controller
        if($_SESSION['tipo_usuario'] != '3')
        {
            // redirecionar para o index            
             header("location:".BASE_URL);
        }            
    }

    public function indexAction()
    {
      
    }

    public function lojistaAction()
    {
        
    }

    public function segmentoAction()
    {
        $listaSegmentos = Application_Model_Segmento::listarTodoSegmentos();
        echo Zend_Json::encode($listaSegmentos);
        die();
    }

    public function configuracoesAction()
    {
    }

    public function relatoriovendapdfAction()
    {

        $cabecalhoPDF = "
        <div style='width:100%; height:7%; text-align:center;'>
            <img src='".BASE_URL."/img/logo.png' style='height:10%; float:left; '>
            <br>
            <div style='margin-top:15px; float:left; font-size:15px; font-family:\"verdana\";'>Relatório de vendas dos lojistas</div>
        </div>
        ";

        $conteudoPDF  = "
        <html>
        <head>
        </head>
        <body style='margin-top:20%;float:left;width:100%; font-family:\"verdana\"'>
            <div style='width:100%; height:7%; float:left;'></div>

            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                <b>Informações básicas: </b>
                <br><br>
                <table>
                    <tr><td>Quantidade lojistas:</td><td>xx</td></tr>
                    <tr><td>Quantidade arquitetos:</td><td>xx</td></tr>
                    <tr><td>faturamento clubdecor no mes:</td><td>xx</td></tr>
                    <tr><td>faturamento clubdecor no trimestre:</td><td>xx</td></tr>
                    <tr><td>faturamento clubdecor no ano:</td><td>xx</td></tr>
                </table>
            </div>

            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                <b>Histórico de vendas - mensal</b>
                <br><br>
                <table style='width:100%;border-bottom:1px solid darkgrey; '>
                    <tr style='border-bottom:1px solid black;'>
                        <td>Loja</td>
                        <td>Unidade</td>
                        <td>faturamento clubdecor</td>
                    </tr>
                </table>
            </div>

            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                <b>Histórico de vendas - trimestral</b>
                <br><br>
                <table style='width:100%;border-bottom:1px solid darkgrey; '>
                    <tr style='border-bottom:1px solid black;'>
                        <td>Loja</td>
                        <td>Unidade</td>
                        <td>faturamento clubdecor</td>
                    </tr>
                </table>
            </div>

            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                <b>Histórico de vendas - Anual</b>
                <br><br>
                <table style='width:100%;border-bottom:1px solid darkgrey; '>
                    <tr style='border-bottom:1px solid black;'>
                        <td>Loja</td>
                        <td>Unidade</td>
                        <td>faturamento clubdecor</td>
                    </tr>
                </table>
            </div>
        </body>
        </html>";
        
        $footerPDF    = "
        <div style='float:left; width:40%; height:5%; text-align:left;'>Obs.: as tabelas estão ordenadas na ordem dos lojistas que mais vendem.</div>
        <div style='float:right; width:60%; height:5%;'>".date("d/m/Y - h:m:s")."</div>
        ";

        $cabecalhoPDF = "
        <div style='width:100%; height:7%; text-align:center;'>
            <img src='".BASE_URL."/img/logo.png' style='height:10%; float:left; '>
            <br>
            <div style='margin-top:15px; float:left; font-size:15px; font-family:\"verdana\";'>Relatório de vendas dos lojistas</div>
        </div>
        ";

        $conteudoPDF  = "
        <html>
        <head>
        </head>
        <body style='margin-top:20%;float:left;width:100%; font-family:\"verdana\"'>
            <div style='width:100%; height:7%; float:left;'></div>

            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                <b>Informações básicas: </b>
                <br><br>
                <table>
                    <tr><td>Quantidade lojistas:</td><td>xx</td></tr>
                    <tr><td>Quantidade arquitetos:</td><td>xx</td></tr>
                    <tr><td>faturamento clubdecor no mes:</td><td>xx</td></tr>
                    <tr><td>faturamento clubdecor no trimestre:</td><td>xx</td></tr>
                    <tr><td>faturamento clubdecor no ano:</td><td>xx</td></tr>
                </table>
            </div>

            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                <b>Histórico de vendas - mensal</b>
                <br><br>
                <table style='width:100%;border-bottom:1px solid darkgrey; '>
                    <tr style='border-bottom:1px solid black;'>
                        <td>Loja</td>
                        <td>Unidade</td>
                        <td>faturamento clubdecor</td>
                    </tr>
                </table>
            </div>

            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                <b>Histórico de vendas - trimestral</b>
                <br><br>
                <table style='width:100%;border-bottom:1px solid darkgrey; '>
                    <tr style='border-bottom:1px solid black;'>
                        <td>Loja</td>
                        <td>Unidade</td>
                        <td>faturamento clubdecor</td>
                    </tr>
                </table>
            </div>

            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                <b>Histórico de vendas - Anual</b>
                <br><br>
                <table style='width:100%;border-bottom:1px solid darkgrey; '>
                    <tr style='border-bottom:1px solid black;'>
                        <td>Loja</td>
                        <td>Unidade</td>
                        <td>faturamento clubdecor</td>
                    </tr>
                </table>
            </div>
        </body>
        </html>";
        
        $footerPDF    = "
        <div style='float:left; width:40%; height:5%; text-align:left;'>Obs.: as tabelas estão ordenadas na ordem dos lojistas que mais vendem.</div>
        <div style='float:right; width:60%; height:5%;'>".date("d/m/Y - h:m:s")."</div>
        ";

        $mpdf=new mPDF(); 
        $mpdf->SetHeader($cabecalhoPDF);
        $mpdf->WriteHTML($conteudoPDF);
        $mpdf->SetFooter($footerPDF);
        $mpdf->Output();      

    }

    public function relatorioarquitetopdfAction()
    {

        $retorno = Application_Model_Venda::listarTodosArquitetosRanking();

        echo "<pre>";
        $tabelaConteudo = "<table style='width:100%;font-size:10px;'>";
        for($i = 0 ; $i < count($retorno) ; $i++)
        {
            $tabelaConteudo .= "<tr>
                                    <td width='25%'>".$retorno[$i]['nome_arquiteto']."</td>
                                    <td width='25%'>".$retorno[$i]['email']."</td>
                                    <td width='20%'>".$retorno[$i]['telefone']."</td>
                                    <td width='15%'>".$retorno[$i]['total_tranzacoes']."</td>
                                    <td width='15%'>".$retorno[$i]['pontuacao']."</td>
                                </tr>";
        }
        $tabelaConteudo .= "</table>";        

        $cabecalhoPDF = "
        <div style='width:100%; height:7%; text-align:center;'>
            <img src='".BASE_URL."/img/logo.png' style='height:10%; float:left; '>
            <br>
            <div style='margin-top:15px; float:left; font-size:15px; font-family:\"verdana\";'>Relatório de ranking arquitetos</div>
        </div>
        ";

        $conteudoPDF  = "
        <html>
        <head>
        </head>
        <body style='margin-top:20%;float:left;width:100%; font-family:\"verdana\"'>
            <div style='width:100%; height:7%; float:left;'></div>

            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                <b>Ranking arquitetos</b>
                <br><br>
                <table style='width:100%;border-bottom:1px solid darkgrey; '>
                    <tr style='border-bottom:1px solid black;'>
                        <td width='25%'>Arquiteto</td>
                        <td width='25%'>email</td>
                        <td width='20%'>Tel. movel</td>
                        <td width='15%'>Total transações</td>
                        <td width='15%'>Pontuação</td>
                    </tr>
                </table>

                {$tabelaConteudo}

            </div>

        </body>
        </html>";
        
        $footerPDF    = "
        <div style='float:left; width:40%; height:5%; text-align:left;'>Obs.: A lista de arquitetos está ordenada por quantidade do valor de compra do arquiteto.</div>
        <div style='float:right; width:60%; height:5%;'>".date("d/m/Y - h:m:s")."</div>
        ";

        $mpdf=new mPDF(); 
        $mpdf->SetHeader($cabecalhoPDF);
        $mpdf->WriteHTML($conteudoPDF);
        $mpdf->SetFooter($footerPDF);
        $mpdf->Output();        

    }

    public function regulamentacaoAction()
    {
        
    }

    public function logoutAction()
    {
        // action body
        session_destroy();
        // redirecionar para o index
        header("location:".BASE_URL);        

    }

    public function totalarquitetosAction()
    {
         $retorno =  Application_Model_Usuario::listarTodosPorTipo(1);
         echo $retorno->count();
    }

    public function totallojistasAction()
    {
         $retorno =  Application_Model_Usuario::listarTodosPorTipo(2);
         echo $retorno->count();
    }

    public function cadastrarsegmentoAction()
    {

        $data       = json_decode(file_get_contents("php://input"));
        $arrayDados = $data->arrDados;

        $dados      = array(
            "id"                =>  null,
            "segmento"          =>  $arrayDados->segmento,
        );
    
        $retorno = Application_Model_Segmento::cadastrar_segmento($dados);

        echo Zend_Json::encode($retorno);
        die();
    }

    public function cadastrarlojistaAction()
    {
        $data       = json_decode(file_get_contents("php://input"));
        $arrayDados = $data->arrDados;

        $dados      = array(
            'id'        => null,
            'email'     => $arrayDados->email,
            'senha'     => sha1($arrayDados->senha),
            'razao_social'  => $arrayDados->razao_social,
            'nome'      => $arrayDados->nome_fantasia,
            'segmento'  => (int)$arrayDados->segmento,
            'cpf'       => $arrayDados->cnpj,
            'endereco'  => $arrayDados->endereco,
            'site'      => $arrayDados->site,
            'data_nascimento' => $arrayDados->data_fundacao,
            'telefone'  => $arrayDados->tel_contato,
            'tipo_pessoa'    => 'pj',
            'tipo_usuario'   => 2,
            'num_end'   => 0,  
            'bairro'    => '', 
            'cidade'    => '', 
            'estado'    => ''
        );

	$regulamentacao = Application_Model_Regulamentacao::listarregulamentacao();
	
	// enviando email para o lojista
	mail($arrayDados->email, 'Bem vindo lojista ao clubdecor', '
	
	Olá, '.$arrayDados->nome_fantasia.'. Seja-bem vindo ao Clubdecor.
 
	Você está recebendo login, senha e regulamento do seu clube de vantagens.
	Este regulamento tem como finalidade estipular as regras e condições gerais para a participação no nosso programa. Aproveite.
	
	seu login: '.$arrayDados->email.'
	sua  senha: '.$arrayDados->senha.'
	
	
	
	'.$regulamentacao->regulamentacao_lojista.'
	');

        //cadastrando lojista
        $retorno    = Application_Model_Usuario::cadastrar($dados);

        // pegando id do lojista
        $idUsuario  = Application_Model_Usuario::logar($dados);

        for($i = 0 ; $i < count($arrayDados->unidade) ; $i++){
            
            $dados  = array(
                'descricao'  => $arrayDados->unidade[$i],
                'id_lojista'  => $idUsuario->id
                );
            //vinculando as unidades ao lojista
            $retorno_unidade = Application_Model_Unidade::cadastrar($dados);

            $dados  = array();
        }

        echo Zend_Json::encode($retorno,$idUsuario,$retorno_unidade);
        die();        
    }

    public function listarcampanhaAction()
    {
        $retorno =  Application_Model_Regulamentacao::listarregulamentacao();

        $obj_retorno = new stdclass();

        $obj_retorno->data_inicial_campanha = $retorno->data_inicial;
        $obj_retorno->data_final_campanha   = $retorno->data_final;
        $obj_retorno->descricao_campanha    = $retorno->descricao;
        $obj_retorno->regulamentacao_lojista= $retorno->regulamentacao_lojista;
        $obj_retorno->bloquear_venda        = $retorno->bloquear_venda;

        if($retorno->bloquear_venda == 0)
        {
            $obj_retorno->texto_venda = 'Bloquear vendas';
            $obj_retorno->valor_bloq  = 1;
        }else{
            $obj_retorno->texto_venda = 'Desbloquear vendas';
            $obj_retorno->valor_bloq  = 0;
        }

        echo Zend_Json::encode($obj_retorno);
    }

    public function listarlojistaAction()
    {
        $listaLojista = Application_Model_Usuario::listarTodosPorTipo(2);
        echo Zend_Json::encode($listaLojista);
        die();        
    }

    public function cadastrarregulamentacaoAction()
    {
        $data       = json_decode(file_get_contents("php://input"));
        $arrayDados = $data->arrDados;

        $dados      = array(
            'id'            => null,
            'data_inicial'  => $arrayDados->data_inicial,
            'data_final'    => $arrayDados->data_final,
            'descricao'     => $arrayDados->descricao,
        );

        $retorno = Application_Model_Regulamentacao::atualizarRegulamentacao($dados);
        echo Zend_Json::encode($retorno);        
    }

    public function tabelalistarvendaAction()
    {
        $retorno = Application_Model_Venda::listarTodasVendas();
        echo Zend_Json::encode($retorno);
        die();
    }

    public function tabelalistarankingarquitetoAction()
    {
        $retorno = Application_Model_Venda::listarTodosArquitetosRanking();
        echo Zend_Json::encode($retorno);
        die();
    }

    public function graficovendalucroclubdecorAction()
    {
        // grafico
        $retorno = Application_Model_Venda::graficoFaturamentoPorPeriodo();

        $listaMes   = "[";
        $listaValor = "[";

        for($i = 0 ; $i < count($retorno) ; $i++)
        {
            
            if($i == 0){
                $listaMes .= "'".$retorno[$i]['ano_mes']."'";
                $listaValor  .= $retorno[$i]['valor'];
            }else{
                $listaMes .= ",'".$retorno[$i]['ano_mes']."'";
                $listaValor  .= ", ".$retorno[$i]['valor'];                
            }
            
        }
        $listaMes   .= "]";
        $listaValor .= "]";
	

        echo "<!DOCTYPE html>

                <html lang='pt-BR' style='overflow: hidden;'>
                <head>
                    <meta charset='utf-8'>
                    <meta content='width=device-width, initial-scale=1' name='viewport'>

                    <script src='".BASE_URL."js/Chart.min.js'></script>

                    <style type='text/css'>

                    *{
                        font-family: calibri;        
                    }

                    .box {
                        margin: 0px auto;
                        width: 70%;
                    }

                    .box-chart {
                        width: 100%;
                        margin: 0 auto;
                        padding: 10px;
                    }

                    #GraficoLine{
                        height: 240px !important;
                    }

                    </style>  

                    <script type='text/javascript'>
                        var randomnb = function(){ return Math.round(Math.random()*300)};
                    </script>  

                </head>

                <body>    

                    <div class='box' style='width:90%;float:left;'>
                        <div class='box-chart'>

                            <canvas id='GraficoLine'></canvas>

                            <script type='text/javascript'>

                                var options = {
                                    responsive:true
                                };

                                var data = {
                                    labels: ".$listaMes.",
                                    datasets: [
                                        {
                                            label: 'Dados secundários',
                                            fillColor: '#0094da',
                                            strokeColor: '#4d479d',
                                            pointColor: '#4d479d',
                                            pointStrokeColor: '#fff',
                                            pointHighlightFill: '#fff',
                                            pointHighlightStroke: '#0094da',
                                            data: ".$listaValor."
                                        }
                                    ]
                                };
                                window.onload = function(){

                                    var ctx = document.getElementById('GraficoLine').getContext('2d');
                                    var LineChart = new Chart(ctx).Line(data, options);
                                }  
                            </script>
                        </div>
                    
                    </div>
                </body>

                </html>

                ";
    die();
    }

    public function graficolucroclubdecorporsegmentoAction()
    {
        // grafico
        $retorno = Application_Model_Venda::graficoFaturamentoPorSegmento();

        $listaSegmento = "[";
        $listaValores  = "[";
        for($i = 0 ; $i < count($retorno) ; $i++)
        {
            
            if($i == 0){
                $listaSegmento .= "'".$retorno[$i]['segmento']."'";
                $listaValores  .= $retorno[$i]['valor'];
            }else{
                $listaSegmento .= ",'".$retorno[$i]['segmento']."'";
                $listaValores  .= ", ".$retorno[$i]['valor'];                
            }
            
        }
        $listaSegmento .= "]";
        $listaValores  .= "]";

        echo "<!DOCTYPE html>

                <html lang='pt-BR' style='overflow: hidden;'>
                <head>
                    <meta charset='utf-8'>
                    <meta content='width=device-width, initial-scale=1' name='viewport'>

                    <script src='".BASE_URL."js/Chart.min.js'></script>
                    <script src='".BASE_URL."js/main.js'></script>

                    <style type='text/css'>

                    *{
                        font-family: calibri;        
                    }

                    .box {
                        margin: 0px auto;
                        width: 70%;
                    }

                    .box-chart {
                        width: 100%;
                        margin: 0 auto;
                    }

                    #GraficoBarra{
                        height:250px !important;
                        width:100%;
                    }

                    </style>  

                    <script type='text/javascript'>
                        var randomnb = function(){ return Math.round(Math.random()*300)};
                    </script>  

                </head>
                <body>    
                    <div class='box' style='width:90%;float:left;'>

                        <div class='box-chart'>

                            <canvas id='GraficoBarra'></canvas>

                            <script type='text/javascript'>                                        
                                var listaSegmento = [];
                                $(document).ready(function() {  
                                    $.ajax({
                                        url: BASE_URL+'clubdecor/graficolucroclubdecorporsegmento/?exibirJson=1',
                                        success: function (data) {
                                            var obj = JSON.parse(data);
                                            var i = 0;
                                            obj.forEach(function(o, index){
                                                listaSegmento[i] = o.segmento;
                                                i++;
                                            });
                                        },
                                    });             
                                });

                                var options = {
                                    responsive:true
                                };

                                var data = {
                                    labels: ".$listaSegmento.",
                                    datasets: [
                                        {
                                            label: 'Dados secundários',
                                            fillColor: '#f98e33',
                                            strokeColor: '#f98e33',
                                            highlightFill: '#ee1c25',
                                            highlightStroke: '#ee1c25',
                                            data: ".$listaValores."
                                        }
                                    ]
                                };                

                                window.onload = function(){
                                    var ctx = document.getElementById('GraficoBarra').getContext('2d');
                                    var BarChart = new Chart(ctx).Bar(data, options);
                                }           
                            </script>

                        </div>
                    
                    </div>
                </body>

                </html>";
        die();
    }

    public function listacontatoAction()
    {
        if($_GET['param'] == 'exibirLojista')
        {
            $retorno = Application_Model_Usuario::listarTodosPorTipo(2);
            echo Zend_Json::encode($retorno);            
            die();
        }else if ($_GET['param'] == 'exibirArquiteto')
        {
            $retorno = Application_Model_Usuario::listarTodosPorTipo(1);
            echo Zend_Json::encode($retorno);            
            die();
        }

    }

    public function excluirtodasvendasAction()
    {
        $retorno = Application_Model_Venda::excluirTodasVendas();
        echo Zend_Json::encode($retorno);                    
    }

    public function modificarstatusvendaAction()
    {
        $data       = json_decode(file_get_contents("php://input"));
        
        $dados      = array(
            'bloquear_venda'  => $data->arrayDados->valor_status_venda,
        );

        $retorno    = Application_Model_Regulamentacao::atualizarRegulamentacao($dados); 

        die();
    }
    
    public function excluirlojistaAction()
    {
        $data       = json_decode(file_get_contents("php://input"));
        $retorno    = Application_Model_Usuario::deletar($data->id_lojista); 
        die();        
    }    

}



































