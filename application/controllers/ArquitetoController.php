<?php

class ArquitetoController extends Zend_Controller_Action
{

    public function init()
    {
        session_start();
        // validar se o usuario esta logado e se tem autorização para acessar este controller
        if($_SESSION['tipo_usuario'] != '1')
        {
            // redirecionar para o index            
             header("location:".BASE_URL);
        }  

    }

    public function indexAction()
    {
        //TELA        
    }

    public function listarcampanhasAction()
    {
        //TELA
    }

    public function pdfAction()
    {

        //  COLOCANDO MASCARA NOS CAMPOS 
        $data_fundacao  = ($_SESSION['tipo_pessoa'] == 'pj')? "data fundacao: ":"data nascimento: ";
        $cpf_cnpj       = ($_SESSION['tipo_pessoa'] == 'pj')? "CNPJ: ":"CPF: ";


        // PEGANDO DADOS DO BANCO
        $retornoListaLojistas = Application_Model_Venda::listarTodasVendasArquiteto($_SESSION['id']);
        $totalPontuacao       = 0;
        $conteudo_tabela = "<table style='width:100%;font-size:10px; '>";
        for($i = 0; $i < count($retornoListaLojistas); $i++)
        {
            $retornoListaLojistas[$i]["saldoClubDecor"] = str_replace(",","",$retornoListaLojistas[$i]["saldoClubDecor"]);
            $retornoListaLojistas[$i]["saldoClubDecor"] = str_replace(".","",$retornoListaLojistas[$i]["saldoClubDecor"]);
            $retornoListaLojistas[$i]["saldoClubDecor"] = $retornoListaLojistas[$i]["saldoClubDecor"]/100000;

            $conteudo_tabela .= "<tr>
                                    <td width='30%'>".$retornoListaLojistas[$i]['loja']." - ".$retornoListaLojistas[$i]['unidade']."</td>
                                    <td width='30%'>".$retornoListaLojistas[$i]['cliente']."</td>
                                    <td width='15%'>".$retornoListaLojistas[$i]['data']."</td>
                                    <td width='15%'> R$ ".$retornoListaLojistas[$i]['valor']."</td>
                                    <td width='10%'>".(int)$retornoListaLojistas[$i]["saldoClubDecor"]."</td>
                                </tr>";

            $totalPontuacao += $retornoListaLojistas[$i]["saldoClubDecor"];
        }
        $conteudo_tabela .= "</table>";

        $cabecalhoPDF = "
                        <div style='width:100%; height:7%; text-align:center;'>
                            <img src='".BASE_URL."/img/logo.png' style='height:10%; float:left; '>
                            <br>
                            <div style='margin-top:15px; float:left; font-size:15px; font-family:\"verdana\";'>Histórico de compras do arquiteto</div>
                        </div>
        ";

        $conteudoPDF  = "
                        <html>
                        <head>
                        </head>
                        <body style='margin-top:20%;float:left;width:100%; font-family:\"verdana\"'>
                            <div style='width:100%; height:7%; float:left;'></div>
                            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                                <b>Dados do usuário</b>
                                <br><br>
                                <table style='font-size:13px;'>
                                    <tr><td>NOME: </td><td>{$_SESSION['nome']}</td></tr>
                                    <tr><td>EMAIL: </td><td>{$_SESSION['email']}</td></tr>
                                    <tr><td>{$data_fundacao}</td><td>{$_SESSION['data_nascimento']}</td></tr>
                                    <tr><td>TEL. CONTATO: </td><td>{$_SESSION['telefone']}</td></tr>
                                    <tr><td>{$cpf_cnpj}</td><td>{$_SESSION['cpf']}</td></tr>
                                    <tr><td>ENDERECO: </td><td>{$_SESSION['estado']} - {$_SESSION['cidade']}, {$_SESSION['endereco']} Nº {$_SESSION['num_end']}</td></tr>
                                </table>

                            </div>
                            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                                <b>Histórico de compras</b>
                                <br><br>
                                <table style='width:100%;border-bottom:1px solid darkgrey; '>
                                    <tr style='border-bottom:1px solid black;'>
                                        <td width='30%'>Loja - unidade</td>
                                        <td width='30%'>Cliente</td>
                                        <td width='15%'>Data transação</td>
                                        <td width='15%'>Valor transação</td>
                                        <td width='10%'>Pontuação</td>
                                    </tr>
                                </table>
                                {$conteudo_tabela}
                            </div>

                        </body>
                        </html>";
        
        $footerPDF    = "
                        <div style='float:left; width:40%; height:5%; text-align:left;'>Total de pontuação: {$totalPontuacao} </div>
                        <div style='float:right; width:60%; height:5%;'>".date("d:m:Y - h:m:s")."<br> OBS.: Todas as compras listadas são do periodo da vigencia da campanha.<br></div>
                        ";

        $mpdf=new mPDF(); 
        $mpdf->SetHeader($cabecalhoPDF);
        $mpdf->WriteHTML($conteudoPDF);
        $mpdf->SetFooter($footerPDF);
        $mpdf->Output();  
    }

    public function configuracoesAction()
    {
        // action body

    }

    public function logoutAction()
    {
        // action body
        session_destroy();
        // redirecionar para o index

        header("location:".BASE_URL);        

    }

    public function listartodoslojistasAction()
    {


        if($_GET['filtroPeriodo'] == '1' or $_GET['filtroPeriodo'] == 1)
        {
            $data       = json_decode(file_get_contents("php://input"));  
            if($data->arrDados->buscar_periodo == 'default'){
                $retornoListaLojistas = Application_Model_Venda::listarTodasVendasArquiteto($_SESSION['id']);
            }else{        
            $retornoListaLojistas = Application_Model_Venda::listarTodasVendasArquiteto($_SESSION['id'],$data->arrDados->buscar_periodo);
            }
        }else{
            $retornoListaLojistas = Application_Model_Venda::listarTodasVendasArquiteto($_SESSION['id']);
        }

        $totalPontuacao       = 0;

        // COMANDO PARA CALCULAR PONTUAÇÃO DO ARQUITETO E A SUA PONTUAÇÃO TOTAL
        for($i = 0; $i < count($retornoListaLojistas); $i++)
        {
            $retornoListaLojistas[$i]["saldoClubDecor"] = str_replace(",","",$retornoListaLojistas[$i]["saldoClubDecor"]);
            $retornoListaLojistas[$i]["saldoClubDecor"] = str_replace(".","",$retornoListaLojistas[$i]["saldoClubDecor"]);
            $retornoListaLojistas[$i]["saldoClubDecor"] = $retornoListaLojistas[$i]["saldoClubDecor"]/100000;
            $retornoListaLojistas[$i]["saldoClubDecor"] = (int)$retornoListaLojistas[$i]["saldoClubDecor"];

            $totalPontuacao += $retornoListaLojistas[$i]["saldoClubDecor"];
        }

        $retornoListaLojistas[0]['totalPontuacao'] = $totalPontuacao;

        // A DATA FALTANTE PARA  O FINAL DA CAMPANHA
        if(strtotime(date('d/m/Y')) > strtotime($retornoListaLojistas[0]['data_final_regulamentacao'])){
            $retornoListaLojistas[0]['data_final_regulamentacao'] = 'o periodo da campanha expirou.';
        }else{
            $data_final = explode("/",$retornoListaLojistas[0]['data_final_regulamentacao']);
            $data_atual = explode("/",date('d/m/Y'));
            $retornoListaLojistas[0]['data_final_regulamentacao'] = 'Faltam cerca de '.(($data_final[0] - $data_atual[0]) + (($data_final[1] - $data_atual[1])*30) + (($data_final[2] - $data_atual[2])*365)).' dias para o termino da campanha.';
        }

        echo Zend_Json::encode($retornoListaLojistas);
        die();
    }


}








