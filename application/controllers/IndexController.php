<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        session_start();
    }

    public function indexAction()
    {

       if($_SESSION['tipo_usuario'] == '1')
        {
            // redirecionar para o arquiteto
            header("location:".BASE_URL."arquiteto");
        }else if($_SESSION['tipo_usuario'] == '2')
        {
            // redirecionar para o lojista
            header("location:".BASE_URL."lojista");
        }else if($_SESSION['tipo_usuario'] == '3')
        {
            // redirecionar para o clubdecor
            header("location:".BASE_URL."clubdecor");
        }

    }

    public function recuperarsenhaAction()
    {
        //TELA
    }

    public function formCadastrarArquitetoAction()
    {
        //TELA     
    }

    public function cadastrarAction()
    {
        $data       = json_decode(file_get_contents("php://input"));

        $arrayDados = $data->arrDados;

        $dados      = array(
            "id"                =>  null,
            "nome"              =>  $arrayDados->nome,
            "email"             =>  $arrayDados->email,
            "senha"             =>  sha1($arrayDados->senha),
            "telefone"          =>  $arrayDados->telefone,
            "endereco"          =>  $arrayDados->endereco,
            "cpf"               =>  $arrayDados->cpf,                
            "data_nascimento"   =>  $arrayDados->nascimento,                
            "num_end"           =>  $arrayDados->num_end,                
            "bairro"            =>  $arrayDados->bairro,                
            "cidade"            =>  $arrayDados->cidade,                
            "estado"            =>  $arrayDados->estado,                
            "tipo_pessoa"       =>  "pf",                
            "tipo_usuario"      =>  '1',                
            "razao_social"      =>  '',     
            "segmento"          =>   0       
        );

        $retorno = Application_Model_Usuario::cadastrar($dados);

        // buscar usuario registrado
        $dados    = array(
                'email'  => $arrayDados->email,
                'senha'  => sha1($arrayDados->senha)
            );
        
        $retorno  = Application_Model_Usuario::logar($dados);

        //ABRINDO SESSÃO
        if($retorno)
        {
            $_SESSION['id']             = $retorno->id;
            $_SESSION['nome']           = $retorno->nome;
            $_SESSION['email']          = $retorno->email;
            $_SESSION['telefone']       = $retorno->telefone;
            $_SESSION['endereco']       = $retorno->endereco;
            $_SESSION['cpf']            = $retorno->cpf;
            $_SESSION['num_end']        = $retorno->num_end;
            $_SESSION['data_nascimento']= $retorno->data_nascimento;
            $_SESSION['bairro']         = $retorno->bairro;
            $_SESSION['cidade']         = $retorno->cidade;
            $_SESSION['estado']         = $retorno->estado;
            $_SESSION['tipo_pessoa']    = $retorno->tipo_pessoa;
            $_SESSION['tipo_usuario']   = $retorno->tipo_usuario;
        }

        echo Zend_Json::encode($retorno);
    }

    public function logarusuarioAction()
    {
      
      $data     = json_decode(file_get_contents("php://input"));
      
      $dados    = array(
            'email'  => $data->arrDados->email,
            'senha'  => sha1($data->arrDados->senha)
        );
      $retorno  = Application_Model_Usuario::logar($dados);

      //ABRINDO SESSÃO
      if($retorno)
      {
        $_SESSION['id']             = $retorno->id;
        $_SESSION['nome']           = $retorno->nome;
        $_SESSION['email']          = $retorno->email;
        $_SESSION['telefone']       = $retorno->telefone;
        $_SESSION['endereco']       = $retorno->endereco;
        $_SESSION['num_end']        = $retorno->num_end;
        $_SESSION['cpf']            = $retorno->cpf;
        $_SESSION['data_nascimento']= $retorno->data_nascimento;
        $_SESSION['bairro']         = $retorno->bairro;
        $_SESSION['cidade']         = $retorno->cidade;
        $_SESSION['estado']         = $retorno->estado;
        $_SESSION['tipo_pessoa']    = $retorno->tipo_pessoa;
        $_SESSION['tipo_usuario']   = $retorno->tipo_usuario;
      }

      echo Zend_Json::encode($retorno);
      die();
    }

    public function regulamentacaoAction()
    {
        // action body
    }

    public function regulamentacaoarquitetoAction()
    {
        $retorno = Application_Model_Regulamentacao::listarregulamentacao();
        echo Zend_Json::encode($retorno);
    }


}













