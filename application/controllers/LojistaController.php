<?php

class LojistaController extends Zend_Controller_Action
{

    public function init()
    {
        session_start();  
        // validar se o usuario esta logado e se tem autorização para acessar este controller
        if($_SESSION['tipo_usuario'] != '2')
        {
            // redirecionar para o index            
             header("location:".BASE_URL);
        }          
    }

    public function indexAction()
    {
        // TELA
      
    }

    public function registrarvendaAction()
    {
        // TELA
        $retorno =  Application_Model_Regulamentacao::listarregulamentacao();
        if($retorno->bloquear_venda == 1 or $retorno->bloquear_venda == '1')
        {

            echo "<style>  .formulario-cadastro-venda{display:none;} </style>";
            echo "<script> alert('O formulario de cadastro de venda foi bloqueado pelo administrador!'); </script>";

        }
        
    }

    public function relatorioarquitetopdfAction()
    {

        // --- LISTA COM TODAS AS UNIDADES --- 
        $retornoUnidades = Application_Model_Unidade::listar($_SESSION['id']);
        $listaUnidades = "";
        foreach($retornoUnidades as $retorno)
        {
            $listaUnidades .= $retorno['descricao'].", ";
        }
        $listaUnidades = substr($listaUnidades,0,-2);


        $tabelaArquiteto = "<table style='width:100%'>";

        $retorno = Application_Model_Venda::listarTodosArquitetosLojista($_SESSION['id']);

        for($i = 0; $i< count($retorno);$i++)
        {
            $saldoClubDecor                   = $retorno[$i]['valor'];
            $saldoClubDecor                   = (int)(($saldoClubDecor*13)/1000);
            $ultimos_dois_caracteres          = substr($saldoClubDecor, -2);
            $restante_dos_caracteres          = substr($saldoClubDecor, 0, (strlen($saldoClubDecor) - 2) );
            $valor                            = $retorno[$i]['valor'];
            $ultimos_dois_caracteres_valor    = substr($valor, -2);
            $restante_dos_caracteres_valor    = substr($valor, 0, (strlen($valor) - 2) );
            $retorno[$i]['saldoClubDecor']  = $restante_dos_caracteres.",".$ultimos_dois_caracteres;
            $retorno[$i]['valor']           = $restante_dos_caracteres_valor.",".$ultimos_dois_caracteres_valor;

            $tabelaArquiteto .= "
                                <tr>
                                    <td width='50%'>".$retorno[$i]['nome']."</td>
                                    <td width='25%'>R$ ".$retorno[$i]['valor']."</td>
                                    <td width='25%'>R$ ".$retorno[$i]['saldoClubDecor']."</td>
                                </tr>
                                ";    

        }

        $tabelaArquiteto .= "</table>";


        $cabecalhoPDF = "
                        <div style='width:100%; height:7%; text-align:center;'>
                            <img src='".BASE_URL."/img/logo.png' style='height:10%; float:left; '>
                            <br>
                            <div style='margin-top:15px; float:left; font-size:15px; font-family:\"verdana\";'>Ranking arquitetos</div>
                        </div>
        ";

        $conteudoPDF  = "
                        <html>
                        <head>
                        </head>
                        <body style='margin-top:20%;float:left;width:100%; font-family:\"verdana\"'>
                            <div style='width:100%; height:7%; float:left;'></div>
                            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                                <b>Dados do lojista</b>
                                <br><br><br>
                                <table style='font-size:13px;'>
                                    <tr><td>Loja: </td><td>{$_SESSION['nome']}</td></tr>
                                    <tr><td>Unidades: </td><td>{$listaUnidades}.</td></tr>
                                    <tr><td>TEL. MOVEL: </td><td>{$_SESSION['telefone']}</td></tr>
                                    <tr><td>CNPJ: </td><td>{$_SESSION['cpf']}</td></tr>
                                    <tr><td>ENDERECO: </td><td>{$_SESSION['estado']} - {$_SESSION['cidade']}, {$_SESSION['endereco']} Nº {$_SESSION['num_end']}</td></tr>
                                </table>

                            </div>
                            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                                <b>Ranking arquitetos</b>
                                <br><br>
                                <table style='width:100%;border-bottom:1px solid darkgrey; '>
                                    <tr style='border-bottom:1px solid black;'>
                                        <td width='50%'>arquiteto</td>
                                        <td width='25%'>Total transações</td>
                                        <td width='25%'>Saldo clubdecor</td>
                                    </tr>
                                </table>

                                {$tabelaArquiteto}

                            </div>

                        </body>
                        </html>";
        
        $footerPDF    = "
                        <div style='float:left; width:40%; height:5%; text-align:left;'></div>
                        <div style='float:right; width:60%; height:5%;'>".date("d/m/Y - h:m:s")."</div>
                        ";

        $mpdf=new mPDF(); 
        $mpdf->SetHeader($cabecalhoPDF);
        $mpdf->WriteHTML($conteudoPDF);
        $mpdf->SetFooter($footerPDF);
        $mpdf->Output();  
    }

    public function relatoriovendapdfAction()
    {

        // --- LISTA COM TODAS AS UNIDADES --- 
        $retornoUnidades = Application_Model_Unidade::listar($_SESSION['id']);
        $listaUnidades = "";
        foreach($retornoUnidades as $retorno)
        {
            $listaUnidades .= $retorno['descricao'].", ";
        }
        $listaUnidades = substr($listaUnidades,0,-2);

        // --- LISTA VENDA --- 
        $retorno      = Application_Model_Venda::listarTodasVendasLojista($_SESSION['id']);
        $tabelaVenda  = "<table style='width:100%; font-size:10px;'>";
        $var_total_clubdecor = 0;
        $var_total    = 0;
        $somatorio_valor = 0;
        for($i = 0; $i< count($retorno);$i++)
        {

            $saldoClubDecor             = $retorno[$i]['saldoClubDecor'];
            $saldoClubDecor             = (int)(($saldoClubDecor*13)/1000);
            $ultimos_dois_caracteres    = substr($saldoClubDecor, -2);
            $restante_dos_caracteres    = substr($saldoClubDecor, 0, (strlen($saldoClubDecor) - 2) );
            $retorno[$i]['saldoClubDecor']  = $restante_dos_caracteres.",".$ultimos_dois_caracteres;
            $somatorio_valor            = $retorno[$i]['valor_total'];  

            $tabelaVenda .= "
                            <tr>
                                <td width='30%'>".$retorno[$i]['arquiteto']."</td>
                                <td width='10%'>".$retorno[$i]['unidade']."</td>
                                <td width='20%'>".$retorno[$i]['cliente']."</td>
                                <td width='10%'>".$retorno[$i]['data_venda']."</td>
                                <td width='15%'><b>".$retorno[$i]['valor']."</b></td>
                                <td width='15%'>".$retorno[$i]['saldoClubDecor']."</td>
                            </tr>
                            "; 

        }
            // mascarando o valor total clubdecor
            $saldoClubDecor             = $somatorio_valor;
            $saldoClubDecor             = (int)(($saldoClubDecor*13)/1000);
            $ultimos_dois_caracteres    = substr($saldoClubDecor, -2);
            $restante_dos_caracteres    = substr($saldoClubDecor, 0, (strlen($saldoClubDecor) - 2) );
            $var_total_clubdecor        = $restante_dos_caracteres.",".$ultimos_dois_caracteres;

            $saldoClubDecor             = $somatorio_valor;
            $ultimos_dois_caracteres    = substr($saldoClubDecor, -2);
            $restante_dos_caracteres    = substr($saldoClubDecor, 0, (strlen($saldoClubDecor) - 2) );
            $var_total                  = $restante_dos_caracteres.",".$ultimos_dois_caracteres;


        $tabelaVenda .= "</table>";


        $cabecalhoPDF = "
                        <div style='width:100%; height:7%; text-align:center;'>
                            <img src='".BASE_URL."/img/logo.png' style='height:10%; float:left; '>
                            <br>
                            <div style='margin-top:15px; float:left; font-size:15px; font-family:\"verdana\";'>Histórico de vendas do lojista</div>
                        </div>
        ";

        $conteudoPDF  = "
                        <html>
                        <head>
                        </head>
                        <body style='margin-top:20%;float:left;width:100%; font-family:\"verdana\"'>
                            <div style='width:100%; height:7%; float:left;'></div>
                            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                                <b>Dados do lojista</b>
                                <br><br><br>
                                <table style='font-size:13px;'>
                                    <tr><td>Loja: </td><td>{$_SESSION['nome']}</td></tr>
                                    <tr><td>Unidades: </td><td>{$listaUnidades}.</td></tr>
                                    <tr><td>TEL. MOVEL: </td><td>{$_SESSION['telefone']}</td></tr>
                                    <tr><td>CNPJ: </td><td>{$_SESSION['cpf']}</td></tr>
                                    <tr><td>ENDERECO: </td><td>{$_SESSION['estado']} - {$_SESSION['cidade']}, {$_SESSION['endereco']} Nº {$_SESSION['num_end']}</td></tr>

                                </table>

                            </div>
                            <div style='float:left;width:100%; height:20%; background-color:#fff'>
                                <b>Histórico de compras</b>
                                <br><br>
                                <table style='width:100%;border-bottom:1px solid darkgrey; '>
                                <tr style='border-bottom:1px solid black;'>
                                    <td width='30%'>arquiteto</td>
                                    <td width='10%'>unidade</td>
                                    <td width='20%'>cliente</td>
                                    <td width='10%'>Data venda</td>
                                    <td width='15%'><b>Valor venda</b></td>
                                    <td width='15%'>saldo clubdecor</td>
                                </tr>

                                </table>

                                {$tabelaVenda}

                            </div>

                        </body>
                        </html>";
        
        $footerPDF    = "
                        <div style='float:left; width:40%; height:5%; text-align:left;'>
                                    Valor total:".$var_total."<br>
                                    Valor total clubdecor:".$var_total_clubdecor."
                        </div>
                        <div style='float:right; width:60%; height:5%;'>".date("d/m/Y - h:m:s")."</div>
                        ";

        $mpdf=new mPDF(); 
        $mpdf->SetHeader($cabecalhoPDF);
        $mpdf->WriteHTML($conteudoPDF);
        $mpdf->SetFooter($footerPDF);
        $mpdf->Output();  
    }

    public function configuracoesAction()
    {
       
    }

    public function logoutAction()
    {
        // action body
        session_destroy();
        // redirecionar para o index
        header("location:".BASE_URL);            
    }

    public function listararquitetoAction()
    {
        $listaSegmentos = Application_Model_Usuario::listarTodosPorTipo(1);
        echo Zend_Json::encode($listaSegmentos);
        die();
    }

    public function cadastrarvendaAction()
    {
        $data       = json_decode(file_get_contents("php://input"));
        $arrayDados = $data->arrDados;       

        //TRATANDO A DATA P/ FORMATO DO BANCO
        $array_data = explode("/",$arrayDados->data);
        $arrayDados->data = $array_data[2]."-".$array_data[1]."-".$array_data[0];

        $dados = array(
            'id'        =>  null,
            'data'      =>  $arrayDados->data,
            'valor'     =>  $arrayDados->valor,
            'id_lojista'=>  (int)$_SESSION['id'],
            'arquiteto' =>  $arrayDados->arquiteto,
            'cliente'   =>  $arrayDados->cliente,
            'unidade'   =>  (int)$arrayDados->unidade
            );
        $retorno = Application_Model_Venda::cadastrar($dados);

        return $retorno;
    }

    public function listarunidadesAction()
    {
        $retorno = Application_Model_Unidade::listar($_SESSION['id']);
        echo Zend_Json::encode($retorno);
        die();
    }

    public function listartodasvendaslojistaAction()
    {   
        if($_GET['filtroPeriodo'] == '1' or $_GET['filtroPeriodo'] == 1)
        {
            $data       = json_decode(file_get_contents("php://input"));  
            if($data->arrDados->buscar_periodo == 'default'){
                $retorno = Application_Model_Venda::listarTodasVendasLojista($_SESSION['id']);
            }else{        
                $retorno = Application_Model_Venda::listarTodasVendasLojista($_SESSION['id'],$data->arrDados->buscar_periodo);
            }
        }else{
            $retorno = Application_Model_Venda::listarTodasVendasLojista($_SESSION['id']);
        }

        //MOSTRAR SALDO DO CLUBDECOR
        for($i = 0; $i< count($retorno);$i++)
        {
            $saldoClubDecor             = $retorno[$i]['saldoClubDecor'];
            $saldoClubDecor             = (int)(($saldoClubDecor*13)/1000);
            $ultimos_dois_caracteres    = substr($saldoClubDecor, -2);
            $restante_dos_caracteres    = substr($saldoClubDecor, 0, (strlen($saldoClubDecor) - 2) );
            //$restante_dos_caracteres    = chunk_split($restante_dos_caracteres, 3, '.');
            //$restante_dos_caracteres    = substr($restante_dos_caracteres,0, -1);

            $retorno[$i]['saldoClubDecor']  = $restante_dos_caracteres.",".$ultimos_dois_caracteres;
        }

        echo Zend_Json::encode($retorno);
        die();
    }

    public function listartodosarquitetosAction()
    {
        //MASCARAR A MOEDA VINDA DO BANCO 
        $retorno = Application_Model_Venda::listarTodosArquitetosLojista($_SESSION['id']);

        for($i = 0; $i< count($retorno);$i++)
        {
            //MASCARA SALDO CLUB DECOR
            $saldoClubDecor             = $retorno[$i]['valor'];
            $saldoClubDecor             = (int)(($saldoClubDecor*13)/1000);
            $ultimos_dois_caracteres    = substr($saldoClubDecor, -2);
            $restante_dos_caracteres    = substr($saldoClubDecor, 0, (strlen($saldoClubDecor) - 2) );

            // MASCARA VALOR
            $valor                            = $retorno[$i]['valor'];
            $ultimos_dois_caracteres_valor    = substr($valor, -2);
            $restante_dos_caracteres_valor    = substr($valor, 0, (strlen($valor) - 2) );

            $retorno[$i]['saldoClubDecor']  = $restante_dos_caracteres.",".$ultimos_dois_caracteres;
            $retorno[$i]['valor']           = $restante_dos_caracteres_valor.",".$ultimos_dois_caracteres_valor;
        }

        echo Zend_Json::encode($retorno);
        die();
    }
    
    
    public function cadastrararquitetoAction()
    {
        $data       = json_decode(file_get_contents("php://input"));
        $arrayDados = $data->arrDados;     
    	
        $dados      = array(
            "id"                =>  null,
            "nome"              =>  $arrayDados->nome,
            "email"             =>  $arrayDados->email,
            "senha"             =>  sha1($arrayDados->senha),
            "telefone"          =>  $arrayDados->telefone,
            "endereco"          =>  $arrayDados->endereco,
            "cpf"               =>  $arrayDados->cpf,                
            "data_nascimento"   =>  $arrayDados->data_nascimento,                
            "num_end"           =>  $arrayDados->num_end,                
            "bairro"            =>  $arrayDados->bairro,                
            "cidade"            =>  $arrayDados->cidade,                
            "estado"            =>  $arrayDados->estado,                
            "tipo_pessoa"       =>  "pf",                
            "tipo_usuario"      =>  '1',                
            "razao_social"      =>  '',     
            "segmento"          =>   0       
        );
                
    	$retorno = Application_Model_Usuario::cadastrar($dados);

	$regulamentacao = Application_Model_Regulamentacao::listarregulamentacao();
	
        // enviando email
	mail($arrayDados->email, 'Seja bem vindo ao sistema clubdecor!', '
	
	Olá, '.$arrayDados->nome.'. Seja-bem vindo ao Clubdecor.

	Você está recebendo login, senha e regulamento do seu clube de vantagens.
	Este regulamento tem como finalidade estipular as regras e condições gerais para a participação no nosso programa. Aproveite.
	
	seu login: '.$arrayDados->email.'
	sua  senha: '.$arrayDados->senha.' 
	
	'.$regulamentacao->descricao.'
	');
    	
    	die();
    }
    
    public function deletarvendaAction()
    {
        $data       = json_decode(file_get_contents("php://input"));    	
    	$retorno = Application_Model_Venda::excluirVendaPorId($data->id_venda);
    	die();
    } 

}























