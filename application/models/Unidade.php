<?php

class Application_Model_Unidade
{

	public function cadastrar($dados)
	{
		// vincular o lojista ao as suas unidades
		$dbtableunidade = new Application_Model_DbTable_Unidade();
		$result 		 = $dbtableunidade->insert($dados);
		return $result;		

	}

	public function listar($idLojista)
	{
		$idLojista = (int)$idLojista;
		$dbtableunidade  = new Application_Model_DbTable_Unidade();
		$result 		 = $dbtableunidade->fetchAll("id_lojista = ".$idLojista);
		return $result;	
	}

}

