<?php

class Application_Model_Regulamentacao
{

	public function listarregulamentacao()
	{
		$dbtableregulamentacao = new Application_Model_DbTable_Regulamentacao();
		$listandoRegulamentacao = $dbtableregulamentacao->fetchRow();
        return $listandoRegulamentacao;
	}

	public function atualizarRegulamentacao($dados)
	{
		$dbtableregulamentacao = new Application_Model_DbTable_Regulamentacao();
		$retornoAtualizacao = $dbtableregulamentacao->update($dados,'id = 0');		
		return $retornoAtualizacao;
	}

}

