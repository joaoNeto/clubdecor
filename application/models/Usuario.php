<?php

class Application_Model_Usuario
{

	public function logar($dados)
	{
		$dbTableUsuario = new Application_Model_DbTable_Usuario();
        $usuario 		= $dbTableUsuario->fetchRow( "email = '".$dados['email']."' and senha = '".$dados['senha']."' " );
        return $usuario;
	}

	public function cadastrar($dados)
	{
		$dbTableUsuario 	= new Application_Model_DbTable_Usuario();
		$dados 				= array(
			'id' 			  => null,
			'nome' 			  => $dados['nome'],
			'email' 		  => $dados['email'],
			'senha' 		  => $dados['senha'],
			'telefone' 		  => $dados['telefone'],
			'endereco' 		  => $dados['endereco'],
			'cpf' 			  => $dados['cpf'],
			'data_nascimento' => $dados['data_nascimento'],
			'num_end' 		  => $dados['num_end'],
			'bairro' 		  => $dados['bairro'],
			'cidade' 		  => $dados['cidade'],
			'estado' 		  => $dados['estado'],
			'tipo_pessoa' 	  => $dados['tipo_pessoa'],
			'tipo_usuario'    => $dados['tipo_usuario'],
			'tipo_usuario'    => $dados['tipo_usuario'],
			'razao_social'    => $dados['razao_social'],
			'segmento'    	  => $dados['segmento']
		);
		$result 			= $dbTableUsuario->insert($dados);
		return $result;
	}

	public function atualizar($dados,$id)
	{
		$id = (int)$id;
		$dbTableUsuario = new Application_Model_DbTable_Usuario();        
        $dados = array(
			'nome' 			  => $dados['nome'],
			'email' 		  => $dados['email'],
			'senha' 		  => $dados['senha'],
			'telefone' 		  => $dados['telefone'],
			'endereco' 		  => $dados['endereco'],
			'cpf' 			  => $dados['cpf'],
			'data_nascimento' => $dados['data_nascimento'],
			'num_end' 		  => $dados['num_end'],
			'bairro' 		  => $dados['bairro'],
			'cidade' 		  => $dados['cidade'],
			'estado' 		  => $dados['estado'],
			'tipo_pessoa' 	  => $dados['tipo_pessoa'],
			'tipo_usuario'    => $dados['tipo_usuario'],
			'tipo_usuario'    => $dados['tipo_usuario']
        );

        return $dbTableUsuario->update( $dados, "id = ".$id);
	}

	public function deletar($id)
	{
		$id = (int)$id;
		$dbTableUsuario = new Application_Model_DbTable_Usuario();
		$resultado = $dbTableUsuario->delete( "id = '".$id."'" );
		return $resultado;
	}

	public function listarTodosPorTipo($tipo_usuario = null)
	{
		$tipo_usuario 	= (int)$tipo_usuario;
		$dbTableUsuario = new Application_Model_DbTable_Usuario();
        $usuario 		= $dbTableUsuario->fetchAll("tipo_usuario = '".$tipo_usuario."'");
        return $usuario;
	}

	public function listarPorId($idUsuario)
	{
		$idUsuario 		= (int)$idUsuario;
		$dbTableUsuario = new Application_Model_DbTable_Usuario();
        $usuario 		= $dbTableUsuario->fetchRow(" id = ".$idUsuario);      
        return $usuario;
	}

}

