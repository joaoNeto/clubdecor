<?php

class Application_Model_Venda
{


	public function cadastrar($dados)
	{
		$dbtablevenda 		= new Application_Model_DbTable_Venda();
		$result 			= $dbtablevenda->insert($dados);
		return $result;
	}

	public function filtrarPorData($dataInicial, $dataFinal)
	{	
		$dbtablevenda 	= new Application_Model_DbTable_Venda();
        $usuario 		= $dbtablevenda->fetchAll(" BETWEEN '".$dataInicial."' AND  '".$dataFinal."' ");
        return $usuario;
	}

	public function listarTodasVendasArquiteto($id_arquiteto,$periodo_filtro = null)
	{

		$id_arquiteto = (int)$id_arquiteto;
		$db = Zend_Db_Table::getDefaultAdapter();
		if($periodo_filtro != null)
		{
			$select = $db->select()
			             ->from('viewlistarusuario')
			             ->where("arquiteto = ".$id_arquiteto." and bd_date between '".date('Y-m-d',strtotime("-".$periodo_filtro." month"))."' and '".date('Y-m-d')."'");
			$results = $select->query()->fetchAll();			
		}else{
			$select = $db->select()
			             ->from('viewlistarusuario')
			             ->where('arquiteto = '.$id_arquiteto);
			$results = $select->query()->fetchAll();			
		}
		return $results;

	}

	public function listarTodasVendasLojista($id_lojista,$periodo_filtro = null)
	{
		$id_lojista = (int)$id_lojista;
		$db = Zend_Db_Table::getDefaultAdapter();
		if($periodo_filtro != null)
		{
			$select = $db->select()
			             ->from('viewtabelalojistavenda')
			             ->where("id_lojista = ".$id_lojista." and data_bd between '".date('Y-m-d',strtotime("-".$periodo_filtro." month"))."' and '".date('Y-m-d')."'");
			$results = $select->query()->fetchAll();
		}else{
			$select = $db->select()
			             ->from('viewtabelalojistavenda')
			             ->where('id_lojista = '.$id_lojista);
			$results = $select->query()->fetchAll();
		}
		return $results;
	}

	public function listarTodosArquitetosLojista($id_lojista)
	{
		$id_lojista = (int)$id_lojista;
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
		             ->from('viewrankingarquitetolojista')
		             ->where('id_lojista = '.$id_lojista);
		$results = $select->query()->fetchAll();
		return $results;
	}

	public function listarTodosArquitetosRanking()
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
		             ->from('viewtabelarankingarquitetoclubdecor');
		$results = $select->query()->fetchAll();
		return $results;
	}

	public function listarTodasVendas()
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
		             ->from('viewlistartodasvendaslucrodecor');
		$results = $select->query()->fetchAll();
		return $results;
	}

	public function graficoFaturamentoPorSegmento()
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
		             ->from('graficofaturasegmento');
		$results = $select->query()->fetchAll();
		return $results;
	}

	public function graficoFaturamentoPorPeriodo()
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
		             ->from('graficofaturamentoclubdecorpormes');
		$results = $select->query()->fetchAll();
		return $results;
	}

	public function excluirTodasVendas()
	{
		$dbTableUsuario 		= new Application_Model_DbTable_Venda();
		$dbTableSegmentoLoja 	= $dbTableUsuario->delete();
		return $dbTableSegmentoLoja;
	}	

	public function excluirVendaPorId($id_venda)
	{
		$dbTableUsuario 	= new Application_Model_DbTable_Venda();
		$dbTableSegmentoLoja 	= $dbTableUsuario->delete('id = '.$id_venda);
		return $dbTableSegmentoLoja;	
	}

}