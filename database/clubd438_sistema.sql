-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 12/07/2017 às 18:28
-- Versão do servidor: 5.6.32-78.1
-- Versão do PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `clubd438_sistema`
--

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `graficofaturamentoclubdecorpormes`
--
CREATE TABLE IF NOT EXISTS `graficofaturamentoclubdecorpormes` (
`ano_mes` varchar(71)
,`valor` double
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `graficofaturasegmento`
--
CREATE TABLE IF NOT EXISTS `graficofaturasegmento` (
`segmento` varchar(50)
,`valor_sem_formatacao` double
,`valor` double
);

-- --------------------------------------------------------

--
-- Estrutura para tabela `regulamentacao`
--

CREATE TABLE IF NOT EXISTS `regulamentacao` (
  `data_inicial` varchar(20) NOT NULL,
  `data_final` varchar(20) NOT NULL,
  `descricao` varchar(50000) NOT NULL,
  `regulamentacao_lojista` varchar(5000) NOT NULL,
  `bloquear_venda` int(1) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `regulamentacao`
--

INSERT INTO `regulamentacao` (`data_inicial`, `data_final`, `descricao`, `regulamentacao_lojista`, `bloquear_venda`, `id`) VALUES
('01/01/2017', '09/07/2017', 'REGULAMENTO CAMPANHA CLUBDECOR 2017\n\nA empresa de marketing de incentivo ClubDecor Promoção e Gestão Ltda, estabelecida à rua República do Líbano, 251, sala 2707, bairro do Pina, Recife, Pernambuco, neste ato reconhecida como GESTORA e, de outro lado o (a) interessado (a) qualificado(a) neste instrumento de regulamento oficial, conforme preenchimento dos dados destinados, no site www.clubdecorpe.com.br, por sua livre e espontânea vontade, com fins de participar de ações promocionais, receber material de divulgação, informações e demais avisos para acompanhamento dos processos, passando a ter  acesso a campanhas de marketing de incentivo e relacionamento previamente apresentadas em calendários anual, que será regido pelas cláusulas e condições seguintes:\n\nCLÁUSULA PRIMEIRA – DO OBJETO\n\nO presente regulamento tem por objeto a formalização do CADASTRAMENTO do signatário na condição de ARQUITETO OU DESIGNER DE INTERIORES junto ao sistema de marketing da GESTORA, para que possa ter acesso às ferramentas que se encontram disponíveis no planejamento denominado CLUB DECOR, com a finalidade de participar de produtos e/ou serviços oferecidos junto a diversos públicos, mídias, ações de relacionamento e outros.\n\nCLÁUSULA SEGUNDA – DA CAMPANHA DE RELACIONAMENTO E MARKETING\n\nA campanha CLUBDECOR 2018 será realizada para especificação de produtos e/ou serviços exclusivamente nas empresas associadas ao Clube, que encontram-se listadas no site www.clubdecor.com.br, lista esta que será atualizada sempre que nova empresa se associar.\n\nComo pontuar:\n\nO especificador acumula 1 (um) ponto a cada R$1.000,00 (um mil reais) em vendas de produtos e serviços das empresas associadas ao Clubdecor.\n\nParticipantes:\n\nPoderão participar todos os arquitetos e designers de todo o estado de Pernambuco, que se cadastrarem no Clubdecor.\n\nPeríodo de apuração:\n\nSerão computados todos os pontos acumulados de 02/06/17 a 01/06/17.\n\nPremiação, data e pontos necessários:\n\nPRÊMIOS (DESTINOS)	PONTUAÇÃO	PERÍODO DA VIAGEM\nAmérica do Sul, cultura secular, você escolhe  destino	350	Jul a Out de 2018\nMiami e Caribe, roteiro do decor e glamour	500	Dez de 2018\nParís é uma festa! Maison Object e Semana de Design	850	Set de 2018\nAlém de atingir o objetivo de faturamento, o especificador deverá ter pontuado em no mínimo 50% das lojas associadas do Clubdecor;\n\nO Especificador deve informar o destino e informações necessárias, oficialmente, a GESTORA , com prazo de 60 dias antes da viagem;\n\nO Especificador não poderá transferir sua premiação para terceiros, salvo o profissional que bater a meta duplicada, que poderá levar acompanhante na viagem da campanha escolhida;\n\nO profissional que atingir a meta e optar por não viajar, poderá trocar sua meta batida por : produtos nas lojas associadas, compra de espaço nas mostras da Carlota Comunicação, compra de páginas para divulgação em revista e/ou Anuário ClassCasa;\n\nSó serão aceitas especificações realizadas diretamente pelo especificador ou sob sua ordem, de representante legal de sua empresa;\n\nEm caso de cancelamento da especificação ou inadimplência , a empresa associada fará  a exclusão do valor na soma da pontuação individual do especificador para a campanha;\n\nTodo o controle de faturamento será realizado de forma digital, pela web, tornando obrigatório o cadastramento do especificador individual ou de sua empresa, em portal especialmente desenvolvido para a campanha, com acesso restrito através de senha e login;\n\nAtravés do portal da campanha o especificador poderá acompanhar sua evolução de pontuação de forma restrita, com atualização mensal após o dia 15 de cada mês;\n\nAs condições de entrega do prêmio estarão condicionadas as datas disponibilizadas para o usufruto dos roteiros de viagem. Caso não haja disponibilidade para o especificador, este deverá usufruir de sua premiação na compra de produtos das empresas associadas, mídias da revista ClassCasa ou Anuário ClassCasa, ou ainda portfólio fotográfico;\n\nA operação de reserva e agendamento para usufruto da premiação o é de responsabilidade da EMPRESA GESTORA, não sendo permitida a interferência do especificador na transação diretamente;\n\nOs pontos dos especificadores entre a data inicial e final que não atingirem nenhuma das faixas de premiação serão zerados e sem direito a qualquer reclamação ou utilização de outra forma;\n\nA promoção é exclusivamente para 01 (uma) pessoa/profissional liberal ou um CNPJ, quando empresa constituída, não prevendo divisão;\n\nDurante o decorrer da campanha poderão haver incentivos por parte das empresas associadas, como estímulos a novas especificações, campanhas de mídia, eventos, promoções, mas não obrigatoriamente;\n\nA EMPRESA GESTORA se reserva o direito de, a qualquer tempo, sem prévio aviso, modificar os critérios da CAMPANHA CLUBDECOR ou determinar o seu encerramento, junto ao especificador,  no caso de constatar fraude ou não cumprimento das regras deste regulamento;\n\nA auferição das especificações e pontuação se dará por emissão de nota fiscal, excluindo-se especificações sem emissão de nota fiscal, não ficando condicionado ao prazo de entrega de produtos e/ou serviços;\n\nEm caso de durante o período da campanha haver encerramento de atividades de qualquer empresa associada, o faturamento acumulado do especificador será preservado.', 'Texto regulamentacao lojista', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `segmento`
--

CREATE TABLE IF NOT EXISTS `segmento` (
  `id` int(5) NOT NULL,
  `segmento` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `segmento`
--

INSERT INTO `segmento` (`id`, `segmento`) VALUES
(16, 'segmento 01');

-- --------------------------------------------------------

--
-- Estrutura para tabela `segmentoloja`
--

CREATE TABLE IF NOT EXISTS `segmentoloja` (
  `id` int(5) NOT NULL,
  `id_loja` int(5) NOT NULL,
  `id_segmento` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `unidade`
--

CREATE TABLE IF NOT EXISTS `unidade` (
  `id` int(5) NOT NULL,
  `id_lojista` int(5) NOT NULL,
  `descricao` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `unidade`
--

INSERT INTO `unidade` (`id`, `id_lojista`, `descricao`) VALUES
(27, 116, 'unidade01'),
(28, 116, 'unidade02'),
(29, 116, 'unidade03'),
(30, 117, 'C'),
(31, 118, 'C'),
(32, 119, 'C'),
(33, 120, 'C'),
(34, 121, 'C'),
(35, 122, 'C'),
(36, 123, 'C'),
(37, 124, 'BOA VISTA'),
(38, 124, 'OLINDA'),
(39, 125, 'unidade 01'),
(40, 125, 'unidade 02'),
(41, 127, 'BOA VISTA'),
(42, 127, 'RECIFE'),
(43, 130, 'unidade01'),
(44, 130, 'unidade02'),
(45, 130, 'unidade03'),
(46, 133, 'C'),
(47, 132, ''),
(48, 135, 'C');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(2) NOT NULL,
  `nome` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(300) NOT NULL,
  `senha` varchar(300) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `endereco` varchar(200) NOT NULL,
  `cpf` varchar(19) NOT NULL,
  `data_nascimento` varchar(20) NOT NULL,
  `num_end` int(10) NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `tipo_pessoa` varchar(2) NOT NULL,
  `tipo_usuario` int(2) NOT NULL,
  `razao_social` varchar(200) NOT NULL,
  `segmento` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `email`, `senha`, `telefone`, `endereco`, `cpf`, `data_nascimento`, `num_end`, `bairro`, `cidade`, `estado`, `tipo_pessoa`, `tipo_usuario`, `razao_social`, `segmento`) VALUES
(29, 'Amanda Vila Nova ', 'amandavilanova.arq@gmail.com', '4662f43768d16c25c6b9d98a78417532cb00e47b', '(81) 99813-1521', 'Rua David Perneta ', '073.859.344-39', '14/02/1989', 245, 'Ipsep', 'Recife', 'PE', 'PF', 1, '', 0),
(30, 'CAROL SÁTER', 'arq.csoter@gmail.com', '7418cb796af7d787c6e47cff0755887c1e580992', '(81) 99969-4969', 'Rua Padre Carapuceiro', '039.245.014-32', '27/06/1982', 0, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(31, 'JOANNA NATILDE BRUNO SANTOS', 'diametrodesigninterior@gmail.com', '4102a03a368e7ed33d1acd0e2fd20401eb086509', '(81) 99633-2025', 'Rua Aristides Muniz', '023.841.944-45', '18/06/1977', 121, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(33, 'Silvana Sampaio', 'arquiteturasilsampaio@gmail.com', '6f424c65c4ac23bf29e8d3bd56b2e6e4acce42ac', '(81) 99713-5731', 'Rua ElpÃ­dio Moura', '069.827.644-27', '16/03/1987', 61, 'Matriz', 'VitÃ³ria de Santo AntÃ£o', 'PE', 'PF', 1, '', 0),
(34, 'Jarbas Carneiro Leão Lins', 'jarbaslins@gmail.com', 'b118e69b40427e680a2cc93862cc4a6f71dde4bf', '(81) 99292-9548', 'Rua Poeta Manuel Bandeira', '773.736.424-20', '31/10/1971', 56, 'Imbiribeira', 'Recife', 'PE', 'PF', 1, '', 0),
(35, 'Sandra Lenisa Cavalcanti Vasconcellos ', 'sancellos@gmail.com', 'c8da3ee0db6a0ee75c82d26167e9ee1cca1e057b', '(81) 99965-4364', 'Rua Dona Benvinda de Farias', '439.992.394-68', '30/09/1964', 449, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(36, 'PortoNeves', 'porto_adriana@hotmail.com', '1752b5739655bd40300e89b1325baeb8e0e3ed04', '(81) 98886-0189', 'Avenida Governador Agamenon MagalhÃ£es', '899.491.294-00', '02/04/1974', 350, 'Espinheiro', 'Recife', 'PE', 'PF', 1, '', 0),
(37, 'Carlota Guerra', 'carlota@carlotacomunicacao.com.br', '6acd1640c2d1cd0c2f8c5596a0d12f6eac8c0f82', '(81) 98150-1701', 'Rua CapitÃ£o Rebelinho', '321.296.454-53', '17/01/1967', 600, 'Pina', 'Recife', 'PE', 'PF', 1, '', 0),
(38, 'Romildo Carvalho', 'marco3arquitetos@gmail.com', 'f3c0af07653b3d54233e1c90dff8176cd00d7c57', '(81) 9705-4192', 'Avenida Ministro Marcos Freire', '694.336.584-34', '07/03/1971', 0, 'Casa Caiada', 'Olinda', 'PE', 'PF', 1, '', 0),
(39, 'Elisa Coelho ', 'elisa@elisacoelho.arq.br', 'a7a7fb0c9077b6be398f7061d612e5de9018065e', '(81) 98892-1840', 'Rua JosÃ© Carvalheira ', '01.422.231/402', '07/11/1988', 100, 'Tamarineira', 'Recife', 'PE', 'PF', 1, '', 0),
(40, 'Paulo Gustavo de Oliveira Ramalho', 'paulinhoramalho@gmail.com', 'af6d0ee5aba3f5e6d15ccddaa8080453ae41bf59', '(81) 99245-1388', 'Rua Jaguaribe', '028.501.804-32', '24/06/1978', 0, 'Madalena', 'Recife', 'PE', 'PF', 1, '', 0),
(41, 'Angelo Maranhao', 'angelomaranhao@gmail.com', '874c1d456fe9fb76478cec5b5f67959725b79f0f', '(81) 99615-4885', 'Rua da Baixa Verde ', '354.269.504-15', '09/05/1963', 304, 'Derby', 'Recife', 'PE', 'PF', 1, '', 0),
(42, 'Giovanna Queiroz', 'giocmsq@gmail.com', '152ef6d7aa339d5fdd5791c816b562cbe6bdcf42', '(81) 99242-3232', 'Rua Esmeraldino Bandeira', '624.210.774-00', '09/05/1970', 225, 'GraÃ§as', 'Recife', 'PE', 'PF', 1, '', 0),
(43, 'Pine arquitetura', 'Contato.pinearquitetura@gmail.com', '3db558d9357b9104c7bf6202a570d2ae88153c90', '(81) 99438-4389', 'Rua FÃ©lix de Brito Melo', '089.119.104-65', '09/07/1989', 143, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(44, 'Girlene de Azevedo Lima', 'arquiteturagirlenelima@gmail.com', '62e64f0515b8a6eb947a82d62bc385ab8e9f744e', '(81) 99974-2920', 'Estrada do Arraial', '408.905.624-15', '11/01/1966', 0, 'Tamarineira', 'Recife', 'PE', 'PF', 1, '', 0),
(45, 'Giuliana zorpoli', 'giuliana@pmzarquitetura.com.br', '707949fbee74d11e76db9162d091d60431cd3c41', '(81) 99235-3554', 'Rua Ana Camelo da Silva', '836.315.184-04', '12/05/1976', 0, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(46, 'KARLA RIVAS de carvalho cajado', 'ka.rivas150@gmail.com', '2b2d507fde1c590cfe784b413fc3896903fc9fd6', '(87) 98838-2444', 'Rua Gualter Araripe', '432.112.244-91', '10/04/1965', 150, 'Caminho do Sol', 'Petrolina', 'PE', 'PF', 1, '', 0),
(47, 'EDNISE MAIA', 'ednise@gmail.com', '31ed8df50d253b368003814dde56b37473f74f81', '(81) 99800-8354', 'Rua Jornalista Edson Regis', '047.971.984-51', '18/05/1984', 1337, 'Jardim AtlÃ¢ntico', 'Olinda', 'PE', 'PF', 1, '', 0),
(48, 'Fernanda Muniz Duraes Maia ', 'fernanda@fernandaduraes.com.br', 'c4fc2e6897b6746fe6b6a16456a613300b7be8a5', '(81) 99963-7666', 'Rua Dom JoÃ£o Costa', '390.982.064-68', '11/06/1964', 295, 'TorreÃ£o', 'Recife', 'PE', 'PF', 1, '', 0),
(49, 'DEBORA LEMES ARQUITETA', 'deboralemes@hotmail.com', '620610f932b3a740611b4948469a21bce3101011', '(81) 99756-0306', 'Rua Ministro Nelson Hungria', '280.460.518-37', '26/09/1978', 180, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(50, 'Adriana Diniz', 'adrianadiniz@globo.com', '51f749a7fe33b21ec897bfbcb469d63bbb77fa5d', '(81) 99142-4720', 'Estrada do Encanamento', '246.686.304-97', '24/02/1960', 702, 'Casa Forte', 'Recife', 'PE', 'PF', 1, '', 0),
(51, 'Mirella Dantas de França', 'df.mirella@gmail.com', '57f671f5c74c286d552c6f390669ead04aef86ee', '(81) 99741-3735', 'Rua Professor JosÃ© CÃ¢ndido Pessoa', '056.576.814-01', '26/07/1991', 1445, 'Bairro Novo', 'Olinda', 'PE', 'PF', 1, '', 0),
(52, 'ANA MARIA MENEZES DE CARVALHO PIMENTEL', 'am.arquiteturas@gmail.com', '73ccccf2d40b1cd67d5105302b10a7f0df6d51cc', '(81) 98251-9947', 'Rua Medeiros e Albuquerque', '771.899.224-15', '10/09/1968', 60, 'GraÃ§as', 'Recife', 'PE', 'PF', 1, '', 0),
(53, 'Poligonus Arquitetura Ltda', 'poligonus@poligonus.com', '98dcdf15b90f0424c8ee87b5c70ee3689bf95a4a', '(81) 99609-6169', 'Avenida RepÃºblica do LÃ­bano', '14.215.110/0001-85', '10/10/1986', 251, 'Pina', 'Recife', 'PE', 'PJ', 1, '', 0),
(54, 'FABIANA TEIXEIRA MOTA VALENÇA', 'FABIANATEIXEIRAARQ@TERRA.COM.BR', 'f82d27a0fec42871039eec63e16a3ce4c768d01c', '(81) 99972-6012', 'Rua Professor Augusto Lins e Silva', '440.780.644-34', '19/08/1965', 425, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(55, 'Paula Salgueiro', 'paula@unacasaarquitetura.com.br', '2990f9e47c6b9e79a1dd68a3d148bdcaf3e1ee59', '(81) 99146-7694', 'Avenida Boa Viagem', '669.736.354-15', '20/12/1966', 4988, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(56, 'ROBERTA CARDIM CIDRIM', 'robertacidrimarquitetura@gmail.com', '81325d6d8020e7c086988752028f026b39922b9c', '(81) 9932-7964_', 'Rua JosÃ© de Holanda', '053.250.684-74', '10/04/1984', 801, 'Torre', 'Recife', 'PE', 'PF', 1, '', 0),
(57, 'Celia Beatriz Santana Vieira', 'celiabeatriz14@gmail.com', '542db438d058cfe846e2369e640437bf3883c839', '(81) 98745-1169', 'Rua Agenor Lopes', '020.709.724-02', '14/01/1976', 314, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(58, 'Arquiteturassim ltda', 'Contato@arquiteturassim.com.br', '76dd6ac394e50efcc76820e93e0bf05ab492491f', '(81) 98821-5587', 'Rua DemÃ³crito de Souza Filh', '192.328.620-001', '12/10/1978', 335, 'Madalena', 'Recife', 'PE', 'PJ', 1, '', 0),
(59, 'Fabio Morim', 'fabio@fabiomorim.com.br', '04a243a87f41210f68246bcebb91f2a2698cc1a5', '(81) 99994-8519', 'Avenida Conselheiro Aguiar', '989.565.234-87', '30/09/1974', 1748, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(60, 'M2 Arquitetura e interiores', 'm2arquiteturaeinteriores@gmail.com', '61cf18f9f5b92543fb41982b4c8c8ee65a42decf', '(81) 99921-4712', 'Rua Artur Serpa', '824.998.884-15', '01/03/2015', 497, 'Bairro Novo', 'Olinda', 'PE', 'PF', 1, '', 0),
(61, 'Amanda Carrah de Amorim Uen', 'contato@amandacarrah.com', '60d2d68445683e51f405ab8c864815f26fe56e0f', '(81) 98936-3178', 'Rua Aviador Severiano Lins', '080.396.034-47', '05/10/1992', 73, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(62, 'LUIZ DE SOUZA CANTO NETO', 'luizcanto19@gmail.com', '007d0978e4b8f3529ea7361ae239e2ea680f6332', '(81) 99147-6163', 'Rua Desembargador JoÃ£o Paes', '080.044.774-37', '16/03/1989', 1402, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(63, 'Lucia', 'Luciahelena_porto@hotmail.com', '70dd4af5803c211394f3735755df93b5ba6ee39f', '(81) 97908-3687', 'Rua Professor Ageu MagalhÃ£es', '864.042.374-15', '28/12/1971', 350, 'Parnamirim', 'Recife', 'PE', 'PF', 1, '', 0),
(64, 'Fabiane Cristina de Albuquerque Heráclio ', 'Fh@fabianeheraclio.com.br', 'ef8ede4c805f44339f569ba7ebd25720ebcf49a7', '(81) 3314-7999', 'Avenida Boa Viagem', '036.624.344-66', '01/06/1981', 342, 'Pina', 'Recife', 'PE', 'PF', 1, '', 0),
(65, 'Taciana Feitosa', 'arquitetura@tacianafeitosa.com.br', 'd3486ba002ff0b8545545246210d4637a51533ff', '(81) 99973-2665', 'Rua GuimarÃ£es Peixoto', '547.301.124-15', '02/02/1969', 75, 'Casa Amarela', 'Recife', 'PE', 'PF', 1, '', 0),
(66, 'Nadjãnia Gomes', 'nadjania@gmail.com', '49ab925e84c69ec8990934c9b8075c627ed27bfe', '(81) 8796-5031', 'Rua Bernardino Soares da Silva', '833.359.434-49', '20/10/1974', 405, 'Espinheiro', 'Recife', 'PE', 'PF', 1, '', 0),
(67, 'LENIRA DE MELO', 'lenirademelo@gmail.com', '9e60e5d21ed582adfd11779a73a6a1c263bbe5ed', '(81) 99292-7545', 'Rua Carlos Borromeu', '055.461.364-64', '15/03/1985', 51, 'Encruzilhada', 'Recife', 'PE', 'PF', 1, '', 0),
(68, 'Arcenna - Arquitetura & Iluminacao S/S Ltda', 'contatoarcenna@gmail.com', '9e60e5d21ed582adfd11779a73a6a1c263bbe5ed', '(81) 99292-7545', 'Rua Carlos Borromeu', '18.725.203/0001-00', '31/07/2013', 51, 'Encruzilhada', 'Recife', 'PE', 'PJ', 1, '', 0),
(69, 'TRAÇO COLETIVO ARQUITETURA', 'tracocoletivoarquitetura@gmail.com', '892bf89b68276dbde2157a7ab624ba62e58149ac', '(81) 99650-4498', 'Rua Waldemar Nery Carneiro Monteiro', '033.127.994-06', '01/05/2016', 303, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(70, 'LÃ­via Alencar- LIFE Arquitetura', 'Contato@arquiteturalife.com.br', 'fbe94ec577e8a0000db32b76a87d6b3e66474380', '(81) 99659-3591', 'Avenida Fernando SimÃµes Barbosa', '074.221.494-00', '09/06/1990', 348, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(71, 'Bárbara Cereja', 'arq.barbaracereja@hotmail.com', 'd8002229e96eb6904cff3c0e921012da2192099a', '(81) 99961-5054', 'Avenida RepÃºblica do LÃ­bano', '054.867.734-47', '08/10/1985', 540, 'Pina', 'Recife', 'PE', 'PF', 1, '', 0),
(72, 'Alessandra Viloa Nova de Oliveira', 'alehvilanova@hotmail.com', 'b52234ab0f3e44b354a52e849a8f907e004570a1', '(81) 9649-1367', 'Rua do Fonseca', '899.938.964-20', '03/04/1974', 279, 'Ilha do Retiro', 'Recife', 'PE', 'PF', 1, '', 0),
(73, 'CAROLINA GONÇALVES ', 'carolglemos@gmail.com', '7ce97b0bb72a96d7f693c7a2c4f112478c85512c', '(81) 9167-0399', 'Rua LuÃ­s de Farias Barbosa', '024.124.914-70', '11/04/1976', 120, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(74, 'ESTUDIO NOI ARQUITETURA/ FLAVIA EBRAHIM', 'CONTATO@ESTUDIONOI.COM.BR', 'ebf2bd511574c406d1952c02943ccfc143a98e48', '(81) 99967-1678', 'Avenida RepÃºblica do LÃ­bano', '043.291.654-73', '01/07/1982', 0, 'Pina', 'Recife', 'PE', 'PF', 1, '', 0),
(75, 'Andrea Danzi Russo Leal', 'andreadanzi@hotlink.com.br', '58aeef2314691ce3e423d07d608cac027909b0f3', '(81) 98848-8643', 'Avenida Boa Viagem', '620.567.864-00', '20/02/1972', 2632, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(76, 'Leonardo Brasil', 'leobrarq@gmail.com', 'f114bd54752d86a5283e9085cffbf3d82008c3f9', '(81) 86874-9001', 'Avenida SÃ£o Paulo', '052.688.484-37', '15/05/1984', 921, 'Jardim SÃ£o Paulo', 'Recife', 'PE', 'PF', 1, '', 0),
(77, 'Albuquerque Malvim Arquitetura Ltda. ME', 'contato@albuquerquemalvim.com', '098d0a4945fc6e54d1baa08ec3a58566853af139', '(81) 99240-8611', 'Rua Nunes Machado', '25.424.465/0001-09', '07/03/2016', 97, 'Soledade', 'Recife', 'PE', 'PJ', 1, '', 0),
(78, 'Amanda Pereira', 'contato@amandapereiraarquitetura.com.br', '1a77f87fe7f3a413a92ea11360f2eb3d4c6b7ec5', '(81) 99547-6627', 'Avenida Sul Governador Cid Sampaio', '058.793.954-05', '25/09/1985', 5095, 'Imbiribeira', 'Recife', 'PE', 'PF', 1, '', 0),
(79, 'Marcelo Costa', 'marcelo.costa@superig.com.br', '3b93851b43c3db805252fead63d31de5b7deb7dc', '(81) 87733-7300', 'Avenida Conselheiro Rosa e Silva', '715.706.404-91', '22/07/1975', 1460, 'GraÃ§as', 'Recife', 'PE', 'PF', 1, '', 0),
(80, 'Renata de Barros Braga', 'rb.arquitetura@outlook.com', '3dd9aedeba512e66b868b0549b98a9cd038b4f05', '(81) 99959-1399', 'rua da ConcÃ³rdia', '023.768.744-59', '23/07/1976', 0, 'SÃ£o JosÃ©', 'Recife', 'PE', 'PF', 1, '', 0),
(81, 'Adrina Veloso Brito de Lima', 'adrivelloso@hotmail.com', 'b483e761e6e8646a5fea0af8170d6246787561f2', '(81) 99627-3571', 'Rua BarÃ£o de Vera Cruz', '754.253.784-91', '09/04/1971', 0, 'Campo Grande', 'Recife', 'PE', 'PF', 1, '', 0),
(82, 'Sandra de Lima Elias Bione', 'arq.sandrabione@gmail.com', '6934d3dc24b96ee3d4587de857067c0c20a62a2b', '(81) 99370-6010', 'Rua JoÃ£o Barbalho', '039.053.644-09', '25/10/1981', 0, 'Casa Amarela', 'Recife', 'PE', 'PF', 1, '', 0),
(83, 'Marina Rocha de Albuquerque', 'marina.rocha01@gmail.com', 'd779a75dd5ffac4eb8eb8d98f181d18c97deeed6', '(81) 98891-3089', 'Rua Dois IrmÃ£os', '065.491.224-65', '30/06/1989', 24, 'Apipucos', 'Recife', 'PE', 'PF', 1, '', 0),
(84, 'Tathiane Jordão Nascimento', 'casaviva@casavivaarquitetura.com.br', '82ed10e66d3a976136caff83be7284ebe8b4e509', '(81) 99145-2025', 'Rua SolidÃ´nio Leite', '028.291.984-84', '12/08/1977', 161, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(85, 'LINS E FERBER ARQUITETURA', 'LINSEFERBERARQUITETURA@GMAIL.COM', '65f1256b7546af72989812b7b88a4b8b2aad190c', '(81) 98295-1705', 'Avenida CaxangÃ¡', '031.880.584-70', '09/07/1978', 100, 'Madalena', 'Recife', 'PE', 'PF', 1, '', 0),
(86, 'Tita Barretto', 'contato@urbanreef.com.br', '3c5d3db103ef45a2a34a6239c0d154d552d4ea16', '(81) 99961-7711', 'PraÃ§a Professor Fleming', '032.938.834-78', '21/06/1977', 0, 'Jaqueira', 'Recife', 'PE', 'PF', 1, '', 0),
(87, 'Milene Ferreira Arquitetura e Interiores', 'mileneferreira.arq@gmail.com', 'dbc714121e86bc2c54475170c706cae6074cf14b', '(81) 9262-0660', 'Rua Aviador Severiano Lins', '090.190.314-06', '10/05/1993', 8, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(88, 'Marília Villar', 'primaprojetos@gmail.com', '6d8c73701c8789b9e99c757f405f45c02ccf6c1e', '(81) 99176-8189', 'Rua Professor JÃºlio Ferreira de Melo', '088.861.234-69', '11/06/1990', 916, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(89, 'Camilla Monteiro Barbosa', 'camillamonteiro@live.com', 'df76374450705ecad5b6f04ae1a44e601e8b8618', '(81) 99691-3415', 'Avenida Liberdade', '070.505.394-65', '07/12/1988', 110, 'Sancho', 'Recife', 'PE', 'PF', 1, '', 0),
(90, 'GRANMARMORE', 'PATRICIA@GRANMARMORE.COM.BR', '41754c710ccf8d22b315874656f06e8ffd51bdb3', '(81) 99974-4768', 'RODOVIA BR 232', '03.426.127/0001-65', '17/09/1999', 15, 'SANTO ALEIXO', 'JABOATAO DOS GUARARAPES', 'PE', 'PJ', 1, '', 0),
(91, 'Mariana Stelman Kopke', 'marianaskopke@yahoo.com.br', '6024a16473698350b5f206cb0923b8d16be7fb15', '(81) 99735-1006', 'Rua Poeta Zezito Neves', '072.783.047-33', '20/02/1976', 71, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(92, 'Diogenes Augusto Colaço Martins Ribeiro', 'diogenescolaco@hotmail.com', '7e92826febb29475c7396145b3d8b4748257ef83', '(81) 98798-9845', 'Avenida Parnamirim', '399.550.014-53', '23/08/1964', 327, 'Parnamirim', 'Recife', 'PE', 'PF', 1, '', 0),
(93, 'Kleber Carvalho Arquitetura', 'klebercarvalhoarquitetura@yahoo.com.br', 'e9460f742d040e133dcac1ce0a66ae08af26a442', '(81) 99972-8997', 'Rua Coronel AnÃ­zio Rodrigues Coelho', '711.642.434-20', '18/03/1971', 606, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(94, 'Marcus Vinicius', 'Contato@emeconcreto.com.br', '245a7c3d5d22d2faafd34ced0479229797cb32e4', '(21) 99472-1398', 'Rua Carlos Pereira FalcÃ£o', '082.740.307-02', '12/10/1978', 831, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(95, 'Tânia Maria de Oliveira Santos', 'taniaoliveirasantos@yahoo.com.br', '03b9d98f686dfb8af9e5175c6296b8b0cef1ecad', '(81) 99630-2130', '...', '243.865.104-00', '09/05/1961', 3121, '...', '...', '...', 'PF', 1, '', 0),
(96, 'Jaildo Lima Arq Design', 'jaildo.j@gmail.com', 'c1c83889f1a6c00c8ff97245c522607ce24d9cc8', '(81) 98798-6290', 'Rua Nossa Senhora dos Prazeres', '047.698.124-77', '14/07/1982', 20, 'Dois Carneiros', 'JaboatÃ£o dos Guararapes', 'PE', 'PF', 1, '', 0),
(97, 'Empresa de iluminações ltda', 'Eduardo@emporiodaluz.com.br', '8317c9cf90383db22bcd627bca9c27a343c0606b', '(81) 99839-2882', 'Rua Artur Muniz', '70.224.597/0001-33', '08/09/1981', 224, 'Boa Viagem', 'Recife', 'PE', 'PJ', 1, '', 0),
(98, 'Donne Arquitetura e Consultoria Sociedade Simples ', 'carla@don2e.com.br', 'ea63159154343c5914bc687fd82bf7a67cd956f1', '(81) 99968-3291', 'PraÃ§a Miguel de Cervantes', '97.550.818/0001-10', '10/02/1974', 60, 'Ilha do Leite', 'Recife', 'PE', 'PJ', 1, '', 0),
(99, 'Laboratorio Arquitetura & Design', 'tita@laboratorio.arq.br', 'afb70a35b65487f4ad50437d1ef19ee6b7a6560d', '(81) 99965-5187', 'Avenida Rui Barbosa', '029.409.094-07', '15/07/2016', 830, 'GraÃ§as', 'Recife', 'PE', 'PJ', 1, '', 0),
(100, 'KRISTIANE GRACILIANO', 'k.graciliano@hotmail.com', '40a4fe1fc4ba9b0c3f3c9c0c1e38f79882371d4a', '(81) 99148-2550', 'Rua Dom Manoel da Costa', '847.023.114-68', '20/01/1971', 405, 'Madalena', 'Recife', 'PE', 'PF', 1, '', 0),
(101, 'mabel rios diniz', 'mabelriosarq@gmail.com', '3baffab773ecebdcb7beda6eaef243957be55ccc', '(81) 9961-9859', 'Rua Itanhenga', '733.803.694-20', '22/10/1967', 96, 'TejipiÃ³', 'Recife', 'PE', 'PF', 1, '', 0),
(102, 'AA ARQUITETURA PROJETOS E SERVIÇOS LTDA', 'aaarquitetura.pe@gmail.com', '78acc0d5abde9304905884dde7a0290ec08d2f17', '(81) 99927-2006', 'Avenida RepÃºblica do LÃ­bano', '19.473.663/0001-51', '06/01/2014', 251, 'Pina', 'Recife', 'PE', 'PJ', 1, '', 0),
(103, 'Tarsila Bezerril', 'Tarsilabezerril.arq@gmail.com', '1d88e3364b7d45f6b7c4596513734ece9e11ff18', '(81) 99767-5636', 'Rua Luiz GuimarÃ£es', '042.076.124-16', '27/11/1981', 411, 'PoÃ§o da Panela', 'Recife', 'PE', 'PF', 1, '', 0),
(104, 'Andressa Dantas Rangel', 'contato@andressarangel.com', 'dc3862c278336aa3865c81dc9796415d7c177e23', '(81) 99658-5969', 'Rua Frei JaboatÃ£o', '067.420.464-65', '06/12/1986', 280, 'Torre', 'Recife', 'PE', 'PF', 1, '', 0),
(105, 'Erbert Medeiros ', 'erbertmedeiros@gmail.com', '365b2474a908ddc1cd2486e3392ae39756184631', '(81) 99803-7714', 'Rua Pereira SimÃµes', '012.068.134-07', '04/03/1982', 247, 'Bairro Novo', 'Olinda', 'PE', 'PF', 1, '', 0),
(106, 'Renata calheiros', 'Renatacalheiros@yahoo.com.br', '5d61c7b55e7e918acea5ea54a0c563fd485dee3d', '(81) 98672-8341', 'Rua Alfredo Moreira', '026.672.124-95', '30/01/1977', 51, 'HipÃ³dromo', 'Recife', 'PE', 'PF', 1, '', 0),
(107, 'Mirella Ventura Arquitetura', 'mirellaventura.arq@gmail.com', '645da5ca9b12bf8332d1b190d1d807ff62dfe74e', '(81) 99386-0391', 'Rua Doutor EnÃ©as de Lucena ', '057.315.174-10', '17/04/1985', 0, 'Encruzilhada', 'Recife', 'PE', 'PF', 1, '', 0),
(108, 'Manuella Farias', 'farias.manuella01@gmail.com', '8a79f127bc2bf1cb9d67775e4ce02f24d0dd00c0', '(81) 99615-4827', 'Avenida Visconde de Jequitinhonha', '070.290.914-93', '15/08/1986', 1846, 'Boa Viagem', 'Recife', 'PE', 'PF', 1, '', 0),
(109, 'Cristina de Souza e Da Oliveira', 'Cristinadecoradora@hotmail.com', '3580a326e003b62d11e7cc98d582ecd0f1208f68', '(81) 99978-0024', 'Rua General Vargas', '399.350.604-97', '11/12/1964', 75, 'Iputinga', 'Recife', 'PE', 'PF', 1, '', 0),
(110, 'Theresa Leite', 'anatheresa_sl@hotmail.com', '5d2f688b463f45624b3d5ea15f9336cd29af7bc0', '(11) 98267-8003', 'Rua BarÃ£o do Bananal', '042.320.294-43', '23/03/1982', 0, 'Vila PompÃ©ia', 'SÃ£o Paulo', 'SP', 'PF', 1, '', 0),
(111, 'CECILIA BOECKMANN', 'cecilia@ceciliaboeckmann.arq.br', '70fcb07c443ff62645972147c8158dacfdc3c699', '(81) 8944-9979', 'Rua Frei Caneca', '038.749.214-33', '14/03/1983', 66, 'Janga', 'Paulista', 'PE', 'PF', 1, '', 0),
(112, 'Buarque projetos e construções ltda', 'kesiaf_@hotmail.com', 'dc743c16817a87047c8cea382012c11824e7477f', '(81) 99921-4869', 'Rua 48 n 942 s 12', '06.020.647/0001-50', '21/01/2007', 942, 'Aflitos', 'Recife', 'Pe', 'PF', 1, '', 0),
(114, '', 'luciacarvalho@melhorcom.com.br', 'b1664e4a92d4867b58cfc9c2e6fb579034f9d687', '', '', '', '', 0, '', '', '', '', 3, '', 0),
(115, '', 'carlota@clubdecorpe.com.br', '08e83eaa44bb9d63676fdba8d58d8e5b3451aa9e', '', '', '', '', 0, '', '', '', '', 3, '', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `venda`
--

CREATE TABLE IF NOT EXISTS `venda` (
  `id` int(5) NOT NULL,
  `data` date NOT NULL,
  `valor` varchar(20) NOT NULL,
  `id_lojista` int(20) NOT NULL,
  `arquiteto` varchar(5) NOT NULL,
  `cliente` varchar(300) NOT NULL,
  `unidade` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `viewlistartodasvendaslucrodecor`
--
CREATE TABLE IF NOT EXISTS `viewlistartodasvendaslucrodecor` (
`loja` varchar(300)
,`unidade` varchar(50)
,`lucro` double
,`segmento` varchar(50)
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `viewlistarusuario`
--
CREATE TABLE IF NOT EXISTS `viewlistarusuario` (
`loja` varchar(300)
,`unidade` varchar(50)
,`cliente` varchar(300)
,`data` varchar(10)
,`bd_date` date
,`valor` varchar(20)
,`saldoClubDecor` varchar(20)
,`arquiteto` varchar(5)
,`email` varchar(300)
,`data_final_regulamentacao` varchar(20)
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `viewrankingarquitetolojista`
--
CREATE TABLE IF NOT EXISTS `viewrankingarquitetolojista` (
`nome` varchar(300)
,`valor` double
,`id_lojista` int(20)
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `viewtabelalojistavenda`
--
CREATE TABLE IF NOT EXISTS `viewtabelalojistavenda` (
`id` int(5)
,`arquiteto` varchar(300)
,`unidade` varchar(50)
,`data_venda` varchar(8)
,`data_bd` date
,`cliente` varchar(300)
,`valor` varchar(20)
,`saldoClubDecor` varchar(20)
,`id_lojista` int(20)
,`valor_total` double
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `viewtabelarankingarquitetoclubdecor`
--
CREATE TABLE IF NOT EXISTS `viewtabelarankingarquitetoclubdecor` (
`nome_arquiteto` varchar(300)
,`total_tranzacoes` double
,`pontuacao` double
,`email` varchar(300)
,`telefone` varchar(20)
);

-- --------------------------------------------------------

--
-- Estrutura para view `graficofaturamentoclubdecorpormes`
--
DROP TABLE IF EXISTS `graficofaturamentoclubdecorpormes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`clubd438`@`localhost` SQL SECURITY DEFINER VIEW `graficofaturamentoclubdecorpormes` AS select date_format(`venda`.`data`,'%M / %Y') AS `ano_mes`,sum(replace(replace(`venda`.`valor`,'.',''),',','')) AS `valor` from `venda` group by date_format(`venda`.`data`,'%Y-%m');

-- --------------------------------------------------------

--
-- Estrutura para view `graficofaturasegmento`
--
DROP TABLE IF EXISTS `graficofaturasegmento`;

CREATE ALGORITHM=UNDEFINED DEFINER=`clubd438`@`localhost` SQL SECURITY DEFINER VIEW `graficofaturasegmento` AS select `segmento`.`segmento` AS `segmento`,sum(replace(replace(`venda`.`valor`,'.',''),',','')) AS `valor_sem_formatacao`,sum(replace(replace(`venda`.`valor`,'.',''),',','')) AS `valor` from ((`venda` join `usuario` on((`usuario`.`id` = `venda`.`id_lojista`))) join `segmento` on((`segmento`.`id` = `usuario`.`segmento`))) group by `segmento`.`segmento`;

-- --------------------------------------------------------

--
-- Estrutura para view `viewlistartodasvendaslucrodecor`
--
DROP TABLE IF EXISTS `viewlistartodasvendaslucrodecor`;

CREATE ALGORITHM=UNDEFINED DEFINER=`clubd438`@`localhost` SQL SECURITY DEFINER VIEW `viewlistartodasvendaslucrodecor` AS select `usuario`.`nome` AS `loja`,`unidade`.`descricao` AS `unidade`,sum(replace(replace(`venda`.`valor`,',',''),'.','')) AS `lucro`,`segmento`.`segmento` AS `segmento` from (((`venda` join `usuario` on((`venda`.`id_lojista` = `usuario`.`id`))) join `unidade` on((`venda`.`unidade` = `unidade`.`id`))) join `segmento` on((`usuario`.`segmento` = `segmento`.`id`))) group by `unidade`.`descricao`;

-- --------------------------------------------------------

--
-- Estrutura para view `viewlistarusuario`
--
DROP TABLE IF EXISTS `viewlistarusuario`;

CREATE ALGORITHM=UNDEFINED DEFINER=`clubd438`@`localhost` SQL SECURITY DEFINER VIEW `viewlistarusuario` AS select `usuario`.`nome` AS `loja`,`unidade`.`descricao` AS `unidade`,`venda`.`cliente` AS `cliente`,date_format(`venda`.`data`,'%d/%m/%Y') AS `data`,`venda`.`data` AS `bd_date`,`venda`.`valor` AS `valor`,`venda`.`valor` AS `saldoClubDecor`,`venda`.`arquiteto` AS `arquiteto`,`usuario`.`email` AS `email`,`regulamentacao`.`data_final` AS `data_final_regulamentacao` from (((`venda` join `usuario` on((`venda`.`id_lojista` = `usuario`.`id`))) join `unidade` on((`unidade`.`id` = `venda`.`unidade`))) join `regulamentacao` on((`regulamentacao`.`id` = 0)));

-- --------------------------------------------------------

--
-- Estrutura para view `viewrankingarquitetolojista`
--
DROP TABLE IF EXISTS `viewrankingarquitetolojista`;

CREATE ALGORITHM=UNDEFINED DEFINER=`clubd438`@`localhost` SQL SECURITY DEFINER VIEW `viewrankingarquitetolojista` AS select `usuario`.`nome` AS `nome`,sum(replace(replace(`venda`.`valor`,'.',''),',','')) AS `valor`,`venda`.`id_lojista` AS `id_lojista` from (`venda` join `usuario` on((`venda`.`arquiteto` = `usuario`.`id`))) group by `venda`.`arquiteto`,`venda`.`id_lojista` order by sum(replace(replace(`venda`.`valor`,'.',''),',','')) desc;

-- --------------------------------------------------------

--
-- Estrutura para view `viewtabelalojistavenda`
--
DROP TABLE IF EXISTS `viewtabelalojistavenda`;

CREATE ALGORITHM=UNDEFINED DEFINER=`clubd438`@`localhost` SQL SECURITY DEFINER VIEW `viewtabelalojistavenda` AS select `venda`.`id` AS `id`,`usuario`.`nome` AS `arquiteto`,`unidade`.`descricao` AS `unidade`,date_format(`venda`.`data`,'%d/%m/%y') AS `data_venda`,`venda`.`data` AS `data_bd`,`venda`.`cliente` AS `cliente`,`venda`.`valor` AS `valor`,replace(replace(`venda`.`valor`,',',''),'.','') AS `saldoClubDecor`,`venda`.`id_lojista` AS `id_lojista`,sum(replace(replace(`venda`.`valor`,',',''),'.','')) AS `valor_total` from ((`venda` join `unidade` on((`venda`.`unidade` = `unidade`.`id`))) join `usuario` on((`usuario`.`id` = `venda`.`arquiteto`))) group by `venda`.`arquiteto`,`venda`.`unidade`,`data_venda`,`data_bd`,`venda`.`cliente`,`venda`.`valor`,`saldoClubDecor`,`venda`.`id_lojista`;

-- --------------------------------------------------------

--
-- Estrutura para view `viewtabelarankingarquitetoclubdecor`
--
DROP TABLE IF EXISTS `viewtabelarankingarquitetoclubdecor`;

CREATE ALGORITHM=UNDEFINED DEFINER=`clubd438`@`localhost` SQL SECURITY DEFINER VIEW `viewtabelarankingarquitetoclubdecor` AS select `usuario`.`nome` AS `nome_arquiteto`,sum(replace(replace(`venda`.`valor`,',',''),'.','')) AS `total_tranzacoes`,sum(replace(replace(`venda`.`valor`,',',''),'.','')) AS `pontuacao`,`usuario`.`email` AS `email`,`usuario`.`telefone` AS `telefone` from (`venda` join `usuario` on((`usuario`.`id` = `venda`.`arquiteto`))) group by `usuario`.`nome`;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `regulamentacao`
--
ALTER TABLE `regulamentacao`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `segmento`
--
ALTER TABLE `segmento`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `segmentoloja`
--
ALTER TABLE `segmentoloja`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `unidade`
--
ALTER TABLE `unidade`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `usuario`
--
ALTER TABLE `usuario`
  ADD UNIQUE KEY `id` (`id`);

--
-- Índices de tabela `venda`
--
ALTER TABLE `venda`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `regulamentacao`
--
ALTER TABLE `regulamentacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `segmento`
--
ALTER TABLE `segmento`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de tabela `segmentoloja`
--
ALTER TABLE `segmentoloja`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `unidade`
--
ALTER TABLE `unidade`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT de tabela `venda`
--
ALTER TABLE `venda`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
