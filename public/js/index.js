$("main").addClass("pre-enter").removeClass("with-hover");
setTimeout(function(){
	$("main").addClass("on-enter");
}, 500);
setTimeout(function(){
	$("main").removeClass("pre-enter on-enter");
	setTimeout(function(){
		$("main").addClass("with-hover");
	}, 50);
}, 3000);

$("h1 a").click(function(){
	$(this).siblings().removeClass("active");
	$(this).addClass("active");
	if ($(this).is("#link-signup")) {
		$("#form-login").removeClass("active");
		$("#intro-login").removeClass("active");
		setTimeout(function(){
			$("#form-signup").addClass("active");
			$("#intro-signup").addClass("active");
		}, 50);
	} else {
		$("#form-signup").removeClass("active");
		$("#intro-signup").removeClass("active");
		setTimeout(function(){
			$("#form-login").addClass("active");
			$("#intro-login").addClass("active");
		}, 50);
	}
});

$('.data_nascimento_mask').mask('00/00/0000');
$('.telefone_mask').mask('(00) 00000 - 0000');
$('.numero_mask').mask('00000');
$('.cpf_mask').mask('000.000.000 - 00');

$('#pf').click(function(){
    $('.cpf_mask').mask('000.000.000 - 00');
	$("#cad_cpf").attr("placeholder", "digite seu cpf").placeholder();
});

$('#pj').click(function(){
    $('.cpf_mask').mask('99.999.999/9999-99');    
	$("#cad_cpf").attr("placeholder", "digite o CNPJ da empresa").placeholder();
});

$('#regulamentacao').click(function(){
            if ($(this).prop("checked")) {
                $('#botao-cadastrar-arquiteto').css("backgroundColor","#4d479d");
                $('#botao-cadastrar-arquiteto').css("cursor","pointer");
            } else {
                $('#botao-cadastrar-arquiteto').css("backgroundColor","#c9c8c6");
                $('#botao-cadastrar-arquiteto').css("cursor","default");
            }

});
/* --------------------- ANGULAR JS --------------------- */
angular.module('myApp', []).controller('namesCtrl', function($scope,$http) {    


    $http.get(BASE_URL+"index/regulamentacaoarquiteto")
    .success(function(data){
        $scope.textoRegulamentacao = data.descricao;
    });

    // LOGANDO ARQUITETO
    $scope.logarArquiteto = function(){
    	var validado = true;
    	//desabilitar botao e colocar nome de processando
    	$('#botao-logar-usuario').prop( "disabled", true );
    	$('#botao-logar-usuario').css("backgroundColor","#4774a2");
    	$('#botao-logar-usuario').val("Validando...");

    	var arrDados =  {
    		'email' : $scope.email_clubdecor ,
    		'senha' : $scope.senha_clubdecor
    	};

    	if(validado){
    		$http.post(BASE_URL+'index/logarusuario',{'arrDados': arrDados})
            .success(function(data, status){
                $('#botao-logar-usuario').prop( "disabled", false );
                $('#botao-logar-usuario').css("backgroundColor","#4774a2");
                $('#botao-logar-usuario').val("Logado");

                if(data.tipo_usuario == 1){
                    // redirecionar para arquiteto
                    window.location.href = BASE_URL+"arquiteto";
                }else if(data.tipo_usuario == 2){
                    // redirecionar para lojista
                    window.location.href = BASE_URL+"lojista";
                } else if (data.tipo_usuario == 3){
                    // redirecionar para clubdecor
                    window.location.href = BASE_URL+"clubdecor";
                }else{
                    alert('usuario informado não existe');
                    $('#botao-logar-usuario').val("Entrar na minha conta");
                }

            }).error(function(data, status) {
                $('#botao-logar-usuario').prop( "disabled", false );
                $('#botao-logar-usuario').css("backgroundColor","#4774a2");
                $('#botao-logar-usuario').val("Entrar na minha conta");
            });
    	}

    }

    $scope.cadastrarArquiteto = function()
    {
    	var validado = true;

    	$('#botao-cadastrar-arquiteto').val('Processando...');
    	
    	/* --------------------------- FAZENDO TODAS AS VALIDAÇÕES NECESSÁRIAS ------------------------------ */

    	if($( '#regulamentacao' ).prop( "checked" ) == false)
    	{
    		validado = false;
    		alert('voce nao aceitou os termos!');
    	}

    	/* ----------------------------- INSERINDO O USUARIO NA BASE DE DADOS -------------------------------- */
    	if(validado)
    	{

            var arrDados =  {
                'nome'      : $scope.cad_nome ,
                'email'     : $scope.cad_email ,
                'senha'     : $scope.cad_senha ,
                'nascimento': $scope.cad_nascimento ,
                'telefone'  : $scope.cad_telefone ,
                'cpf'       : $scope.cad_cpf ,
                'endereco'  : $scope.cad_endereco ,
                'num_end'   : $scope.cad_num_end ,
                'bairro'    : $scope.cad_bairro ,
                'cidade'    : $scope.cad_cidade ,
                'estado'    : $scope.cad_estado
            };

            $http.post(BASE_URL+'index/cadastrar',{'arrDados': arrDados})
            .success(function(data, status){
                $('#botao-cadastrar-arquiteto').val('Cadastrado com sucesso!');       
                window.location.href = BASE_URL+"arquiteto";
                console.log('ok');
            }).error(function(data, status) {
                alert('erro ao cadastrar arquiteto');
                $('#botao-cadastrar-arquiteto').prop( "disabled", false );
                $('#botao-cadastrar-arquiteto').css("backgroundColor","#4774a2");
                $('#botao-cadastrar-arquiteto').val("Cadastrar minha conta");
            });

    	}
        $('#botao-cadastrar-arquiteto').val("Cadastrar minha conta");        

    }

});
